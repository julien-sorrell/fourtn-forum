const children = [

];
const routes = [

    //访问路由“/”,自动重定向到"/home"
    {
        path: '/',
        redirect: '/home'
    },

    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/security/login.vue')
    },

    {
        path: '/loggin',
        name: 'Loggin',
        component: () => import('@/views/security/loggin.vue')
    },

    {
        path: '/searchResult',
        name: 'searchResult',
        component: () => import('@/views/info/searchResult.vue')
    },

    {
        path: '/searchuser',
        name: 'searchuser',
        component: () => import('@/views/info/searchuser.vue')
    },
    {
        path: '/searchbar',
        name: 'searchbar',
        component: () => import('@/views/info/searchbar.vue')
    },
    {
        path: '/searchbbs',
        name: 'searchbbs',
        component: () => import('@/views/info/searchbbs.vue')
    },

    {
        path: '/supplierAllBack',
        name: 'supplierAllBack',
        component: () => import('@/views/security/supplierAllBack.vue')
    },


   

    {
        path: '/test',
        name: 'Test',
        component: () => import('@/views/security/test.vue')
    },

    {
        path: '/home',
        name: 'Home',
        component: () => import('@/views/security/home.vue'),
        children
    },

    {
        path: '/register',
        name: 'Register',
        component: () => import('@/views/security/register.vue')
    },

    {
        path: '/404',
        name: "NotFound",
        component: () => import('@/views/security/404.vue')
    },

    {
        path: '/bbs',
        name: "bbs",
        component: () => import('@/views/info/bbs.vue')
    },

    {
        path: '/ba',
        name: "ba",
        component: () => import('@/views/info/ba.vue')
    },

    {
        path: '/user',
        name: "user",
        component: () => import('@/views/info/user.vue')
    },

    
    {
        path: '/superuser/manage',
        name: 'SuperuserManage',
        component: () => import('@/views/superuser/manage/manage.vue')
    },

    {
        path: '/superuser/module',
        name: 'SuperuserModule',
        component: () => import('@/views/superuser/module/module.vue')
    },

    {
        path: '/admin/manage1',
        name: 'AdminManage1',
        component: () => import('@/views/admin/manage1/manage1.vue')
    },

    {
        path: '/admin/checkba',
        name: 'AdminCheckba',
        component: () => import('@/views/admin/checkba/checkba.vue')
    },

    {
        path: '/bbsadmin',
        name: "bbsadmin",
        component: () => import('@/views/bbsadmin/bbsadmin.vue')
    },

    {
        path: '/bbsadmin/ban',
        name: "ban",
        component: () => import('@/views/bbsadmin/ban.vue')
    },

    
    {
        path: '/bbsadmin/checkreportrecord',
        name: "checkReportRecord",
        component: () => import('@/views/bbsadmin/checkReportRecord.vue')
    },
    {
        path: '/bbsadmin/checkreportreply',
        name: "checkReportReply",
        component: () => import('@/views/bbsadmin/checkReportReply.vue')
    },

];

export const existsRoute = (path) => {
    for (let i = 0; i < routes.length; i++) {
        const r = routes[i];
        if (r.path == path) {
            return true;
        }
        if (r.children && r.children.length) {
            for (let k = 0; k < r.children.length; k++) {
                const sr = r.children[k];
                if (sr.path == path) {
                    return true;
                }
            }
        }
    }
    return false;
};

export default routes;