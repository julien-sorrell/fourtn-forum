package com.fourtn;





import com.fourtn.info.user.user.UserAPI;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@MapperScan({"com.fourtn.*.*.dao"})
public class FourtnApplication {

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(FourtnApplication.class, args);

        UserAPI api = ctx.getBean(UserAPI.class);

        System.out.println(api);


    }

}
