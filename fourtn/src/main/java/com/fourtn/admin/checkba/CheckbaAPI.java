package com.fourtn.admin.checkba;

import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("admin/checkba")
public class CheckbaAPI {
    @Resource
    private CheckbaService checkbaService;

    @GetMapping("")
    public Result checkbaList(CheckbaDto dto) {
        Map<String,Object> page = checkbaService.getUnhandledCheckbaList(dto);
        return Result.success(page);
    }

    @PostMapping("/true")
    public Result handleCheckbaListT(@RequestBody CheckbaDto dto){

            SimpleDateFormat df = new SimpleDateFormat("yyMMdHHmm");
            dto.setBarid(df.format(new Date())+dto.getUserid());
            dto.setRolename("论坛管理员");
            dto.setRoleid(dto.getBarid()+"2");
            checkbaService.insertCheckba(dto);
            checkbaService.updateCheckbaT(dto);
            return Result.success("审核通过");

    }

    @PostMapping("/false")
    public Result handleCheckbaListF(@RequestBody CheckbaDto dto){

            checkbaService.updateCheckbaF(dto);
            return Result.success("审核未通过");

    }


}
