package com.fourtn.admin.checkba;

import com.fourtn.common.PageParam;
import org.springframework.format.annotation.DateTimeFormat;

public class CheckbaDto extends PageParam {
    private String barid;
    private String barname;
    private String bardescription;
    private String userid;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String applytime;
    private Boolean isadopted;
    private String roleid;
    private String rolename;

    public String getBarid() {
        return barid;
    }

    public void setBarid(String barid) {
        this.barid = barid;
    }

    public String getBarname() {
        return barname;
    }

    public void setBarname(String barname) {
        this.barname = barname;
    }

    public String getBardescription() {
        return bardescription;
    }

    public void setBardescription(String bardescription) {
        this.bardescription = bardescription;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getApplytime() {
        return applytime;
    }

    public void setApplytime(String applytime) {
        this.applytime = applytime;
    }

    public Boolean getIsadopted() {
        return isadopted;
    }

    public void setIsadopted(Boolean isadopted) {
        this.isadopted = isadopted;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }
}
