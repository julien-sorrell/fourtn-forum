package com.fourtn.admin.checkba;

import java.util.Map;

public interface CheckbaService {
    Map<String,Object> getUnhandledCheckbaList(CheckbaDto dto);

    void insertCheckba(CheckbaDto dto);

    void updateCheckbaT(CheckbaDto dto);

    void updateCheckbaF(CheckbaDto dto);
}
