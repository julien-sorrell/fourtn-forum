package com.fourtn.admin.checkba.dao;

import com.fourtn.admin.checkba.Checkba;
import com.fourtn.admin.checkba.CheckbaDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface CheckbaDao {

    @Select("select barname, bardescription, userid, applytime, isadopted from barexamined where isadopted is null")
    List<Checkba> getUnhandledCheckbaList(CheckbaDto dto);

    @Insert("insert into bar(barid, barname, bardescription, userid, barhits) values(#{barid}, #{barname}, #{bardescription}, #{userid}, 0)")
    void insertCheckba(CheckbaDto dto);

    @Insert("insert into role(roleid, rolename, barid) values(#{roleid}, #{rolename}, #{barid})")
    void insertRole(CheckbaDto dto);

    @Insert("insert into confirmrole(roleid, userid) values(#{roleid}, #{userid})")
    void insertConfirmRole(CheckbaDto dto);

    @Update("update barexamined set isadopted = true where barname = #{barname}")
    void updateCheckbaT(CheckbaDto dto);

    @Update("update barexamined set isadopted = false where barname = #{barname}")
    void updateCheckbaF(CheckbaDto dto);
}
