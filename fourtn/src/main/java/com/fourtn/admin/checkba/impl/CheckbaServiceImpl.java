package com.fourtn.admin.checkba.impl;

import com.fourtn.admin.checkba.CheckbaDto;
import com.fourtn.admin.checkba.CheckbaService;
import com.fourtn.admin.checkba.dao.CheckbaDao;
import com.fourtn.common.Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

@Service
@Transactional
public class CheckbaServiceImpl implements CheckbaService {
    @Resource
    private CheckbaDao checkbaDao;


    @Override
    public Map<String, Object> getUnhandledCheckbaList(CheckbaDto dto) {
        return Utils.getPage(dto, ()-> checkbaDao.getUnhandledCheckbaList(dto));
    }

    @Override
    public void insertCheckba(CheckbaDto dto) {
        checkbaDao.insertCheckba(dto);
        checkbaDao.insertRole(dto);
        checkbaDao.insertConfirmRole(dto);
    }

    @Override
    public void updateCheckbaT(CheckbaDto dto) {
        checkbaDao.updateCheckbaT(dto);
    }

    @Override
    public void updateCheckbaF(CheckbaDto dto) {
        checkbaDao.updateCheckbaF(dto);
    }
}

