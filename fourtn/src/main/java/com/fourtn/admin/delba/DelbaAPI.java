package com.fourtn.admin.delba;

import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin/delba")
public class DelbaAPI {

    @Resource
    private DelbaService delbaService;

    @DeleteMapping("/{barid}")
    public Result deleteba(@PathVariable String barid){
            delbaService.deleteBa(barid);
            return Result.success();

    }

}
