package com.fourtn.admin.delba;

import com.fourtn.common.PageParam;

public class DelbaDto extends PageParam {
    private String barid;

    public String getBarid() {
        return barid;
    }

    public void setBarid(String barid) {
        this.barid = barid;
    }
}
