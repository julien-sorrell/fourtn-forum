package com.fourtn.admin.delba.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.DeleteMapping;

@Mapper
public interface DelbaDao {
    @Delete("delete reportreply from reportreply,reply where reportreply.replyid = reply.replyid")
    void deleteAllReportreply();

    @Delete("delete reply from reply,bbs where reply.bbsid = bbs.bbsid")
    void deleteAllReply();

    @Delete("delete bbs from bbs,bar where bbs.barid = bar.barid")
    void deleteAllBbs();

    @Delete("delete from bar where barid = #{barid}")
    void deleteBar(String barid);
}
