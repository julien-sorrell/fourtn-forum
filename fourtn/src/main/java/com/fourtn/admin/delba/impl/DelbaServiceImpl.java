package com.fourtn.admin.delba.impl;

import com.fourtn.admin.delba.DelbaService;
import com.fourtn.admin.delba.dao.DelbaDao;
import com.fourtn.bbsadmin.delreply.dao.DelreplyDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class DelbaServiceImpl implements DelbaService {
    @Resource
    private DelbaDao delbaDao;

    @Override
    public void deleteBa(String barid) {
        delbaDao.deleteAllReportreply();
        delbaDao.deleteAllReply();
        delbaDao.deleteAllBbs();
        delbaDao.deleteBar(barid);
    }
}