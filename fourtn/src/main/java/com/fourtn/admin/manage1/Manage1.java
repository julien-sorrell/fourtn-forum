package com.fourtn.admin.manage1;

import com.fourtn.common.PageParam;

public class Manage1 extends PageParam {
    private String roleid;
    private String userid;

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
