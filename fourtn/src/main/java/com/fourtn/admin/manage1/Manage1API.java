package com.fourtn.admin.manage1;

import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/admin/manage1")
public class Manage1API {

    //引入依赖对象
    @Resource //此注解表示从容器中引入依赖对象
    private Manage1Service manage1Service;

    @GetMapping("")
    public Result manage1List(Manage1 dto) {
        Map<String,Object> page = manage1Service.getManage1List(dto);
        return Result.success(page);
    }

    @PostMapping("")
    public Result insertManage1(@RequestBody Manage1 dto){
        Boolean hasrole = manage1Service.isrole(dto.getRoleid());
        if(hasrole) {
            manage1Service.insertManage1(dto);
            return Result.success();
        }
        else {
            return Result.fail(500,"不可新增论坛管理员之外的角色");
        }
    }

    @DeleteMapping("/{roleid}/{userid}")
    public Result deleteManage1(@PathVariable String roleid,@PathVariable String userid){
            manage1Service.deleteManage1(roleid, userid);
            return Result.success();
    }
}
