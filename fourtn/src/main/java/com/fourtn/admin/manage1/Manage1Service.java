package com.fourtn.admin.manage1;

import java.util.Map;

public interface Manage1Service {

    Map<String,Object> getManage1List(Manage1 dto);

    void insertManage1(Manage1 dto);

    void deleteManage1(String roleid,String userid);

    Boolean isrole(String roleid);
}
