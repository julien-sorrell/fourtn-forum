package com.fourtn.admin.manage1.dao;

import com.fourtn.admin.manage1.Manage1;
import com.fourtn.superuser.manage.Manage;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface Manage1Dao {
    @Select("select * from confirmrole where roleid in(select roleid from role where rolename = '论坛管理员')")
    List<Manage> getManage1List(Manage1 dto);

    @Insert("insert into confirmrole(roleid,userid) values(#{roleid}, #{userid})")
    void insertManage1(Manage1 dto);

    @Delete("delete from confirmrole where roleid = #{roleid} and userid = #{userid}")
    void deleteManage1(String roleid,String userid);

    @Select("select count(*) from role where roleid = #{roleid} and rolename = '论坛管理员'")
    Boolean isrole(String roleid);
}
