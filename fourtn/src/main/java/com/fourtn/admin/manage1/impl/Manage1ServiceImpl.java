package com.fourtn.admin.manage1.impl;

import com.fourtn.admin.manage1.Manage1;
import com.fourtn.admin.manage1.Manage1Service;
import com.fourtn.admin.manage1.dao.Manage1Dao;
import com.fourtn.common.Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class Manage1ServiceImpl implements Manage1Service {

    @Resource
    private Manage1Dao manage1Dao;


    @Override
    public Map<String, Object> getManage1List(Manage1 dto) {
        return Utils.getPage(dto, ()-> manage1Dao.getManage1List(dto));
    }

    @Override
    public void insertManage1(Manage1 dto) {
        manage1Dao.insertManage1(dto);
    }

    @Override
    public void deleteManage1(String roleid, String userid) {
        manage1Dao.deleteManage1(roleid,userid);
    }

    @Override
    public Boolean isrole(String roleid) {
        return manage1Dao.isrole(roleid);
    }
}

