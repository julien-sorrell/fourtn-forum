package com.fourtn.bbsadmin.ban;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Ban {
    private String banid;
    private String bannedid;
    private String bannerid;
    private String banreason;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String bantime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String endtime;
    private boolean isvalid;

    public String getBanid() {
        return banid;
    }

    public void setBanid(String banid) {
        this.banid = banid;
    }

    public String getBannedid() {
        return bannedid;
    }

    public void setBannedid(String bannedid) {
        this.bannedid = bannedid;
    }

    public String getBannerid() {
        return bannerid;
    }

    public void setBannerid(String bannerid) {
        this.bannerid = bannerid;
    }

    public String getBanreason() {
        return banreason;
    }

    public void setBanreason(String banreason) {
        this.banreason = banreason;
    }

    public String getBantime() {
        return bantime;
    }

    public void setBantime(String bantime) {
        this.bantime = bantime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public boolean isIsvalid() {
        return isvalid;
    }

    public void setIsvalid(boolean isvalid) {
        this.isvalid = isvalid;
    }
}
