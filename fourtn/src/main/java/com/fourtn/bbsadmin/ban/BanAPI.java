package com.fourtn.bbsadmin.ban;

import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("bbsadmin/ban")
public class BanAPI {
    @Resource
    private BanService banService;

    @PostMapping("/banuser")
    public Result banUser(@RequestBody Ban dto) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dto.setBanid(df.format(new Date())+dto.getBannedid()+dto.getBannerid());
        dto.setIsvalid(true);
        dto.setEndtime(dto.getEndtime().replace('T',' ').substring(0, 19));
        banService.insertBan(dto);
        return Result.success();
    }

    @PostMapping("/cancelban")
    public Result cancelBan(@RequestBody Ban dto) {
        banService.cancelBan(dto);
        return Result.success();
    }

    //用于确认当前用户是否被禁言
    @PostMapping("/getban")
    public Result getBan(@RequestBody Ban ban) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        ban.setEndtime(time);
        return Result.success(banService.getBan(ban));
    }

    //用来查找所有禁言记录
    @RequestMapping("/getallban")
    public Result getAllBan(BanDto dto) {
        return Result.success(banService.getAllBan(dto));
    }
}
