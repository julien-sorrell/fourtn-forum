package com.fourtn.bbsadmin.ban;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class BanDto {
    private String banid;
    private String bannedid;
    private String bannerid;
    private String banreason;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String bantime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String endtime_beg;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String endtime_end;
    private boolean isvalid;

    public String getBanid() {
        return banid;
    }

    public void setBanid(String banid) {
        this.banid = banid;
    }

    public String getBannedid() {
        return bannedid;
    }

    public void setBannedid(String bannedid) {
        this.bannedid = bannedid;
    }

    public String getBannerid() {
        return bannerid;
    }

    public void setBannerid(String bannerid) {
        this.bannerid = bannerid;
    }

    public String getBanreason() {
        return banreason;
    }

    public void setBanreason(String banreason) {
        this.banreason = banreason;
    }

    public String getBantime() {
        return bantime;
    }

    public void setBantime(String bantime) {
        this.bantime = bantime;
    }

    public String getEndtime_beg() {
        return endtime_beg;
    }

    public void setEndtime_beg(String endtime_beg) {
        this.endtime_beg = endtime_beg;
    }

    public String getEndtime_end() {
        return endtime_end;
    }

    public void setEndtime_end(String endtime_end) {
        this.endtime_end = endtime_end;
    }

    public boolean isIsvalid() {
        return isvalid;
    }

    public void setIsvalid(boolean isvalid) {
        this.isvalid = isvalid;
    }
}
