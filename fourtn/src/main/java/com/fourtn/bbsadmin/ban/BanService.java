package com.fourtn.bbsadmin.ban;

import java.util.List;

public interface BanService {
    void insertBan(Ban dto);

    void cancelBan(Ban dto);

    String getBan(Ban ban);

    List<Ban> getAllBan(BanDto dto);
}
