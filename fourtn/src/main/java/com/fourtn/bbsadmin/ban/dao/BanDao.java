package com.fourtn.bbsadmin.ban.dao;

import com.fourtn.bbsadmin.ban.Ban;
import com.fourtn.bbsadmin.ban.BanDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface BanDao {
    @Update("update ban set isvalid = #{isvalid} where bannedid = #{bannedid}")
    void updateCancelBan(Ban dto);

    @Select("select max(endtime) from ban where bannedid = #{bannedid} and endtime >= #{endtime} and isvalid = true")
    String getBan(Ban ban);

    @Insert("insert into ban(banid, bannedid, bannerid, banreason, endtime, isvalid) values(#{banid}, #{bannedid}, #{bannerid}, #{banreason}, #{endtime}, #{isvalid})")
    void insertBan(Ban dto);

    List<Ban> getAllBan(BanDto dto);
}
