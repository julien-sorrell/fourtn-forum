package com.fourtn.bbsadmin.ban.impl;

import com.fourtn.bbsadmin.ban.Ban;
import com.fourtn.bbsadmin.ban.BanDto;
import com.fourtn.bbsadmin.ban.BanService;
import com.fourtn.bbsadmin.ban.dao.BanDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class BanServiceImpl implements BanService {
    @Resource
    private BanDao banDao;

    @Override
    public void insertBan(Ban dto) {
        banDao.insertBan(dto);
    }

    @Override
    public void cancelBan(Ban dto) {
        banDao.updateCancelBan(dto);
    }

    @Override
    public String getBan(Ban ban) {
        return banDao.getBan(ban);
    }

    @Override
    public List<Ban> getAllBan(BanDto dto) {
        return banDao.getAllBan(dto);
    }
}
