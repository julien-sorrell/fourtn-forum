package com.fourtn.bbsadmin.bbsrelated;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class BbsRelated {
    private String bbsid;
    private String barid;
    private String userid;
    private String bbsmark;
    private String bbstitle;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date bbstime;
    private String bbslike;
    private String bbsunlike;
    private Boolean bbstop;
    private Boolean bbsgood;
    private int bbshits;

    public String getBbsid() {
        return bbsid;
    }

    public void setBbsid(String bbsid) {
        this.bbsid = bbsid;
    }

    public String getBarid() {
        return barid;
    }

    public void setBarid(String barid) {
        this.barid = barid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBbsmark() {
        return bbsmark;
    }

    public void setBbsmark(String bbsmark) {
        this.bbsmark = bbsmark;
    }

    public String getBbstitle() {
        return bbstitle;
    }

    public void setBbstitle(String bbstitle) {
        this.bbstitle = bbstitle;
    }

    public Date getBbstime() {
        return bbstime;
    }

    public void setBbstime(Date bbstime) {
        this.bbstime = bbstime;
    }

    public String getBbslike() {
        return bbslike;
    }

    public void setBbslike(String bbslike) {
        this.bbslike = bbslike;
    }

    public String getBbsunlike() {
        return bbsunlike;
    }

    public void setBbsunlike(String bbsunlike) {
        this.bbsunlike = bbsunlike;
    }

    public Boolean getBbstop() {
        return bbstop;
    }

    public void setBbstop(Boolean bbstop) {
        this.bbstop = bbstop;
    }

    public Boolean getBbsgood() {
        return bbsgood;
    }

    public void setBbsgood(Boolean bbsgood) {
        this.bbsgood = bbsgood;
    }

    public int getBbshits() {
        return bbshits;
    }

    public void setBbshits(int bbshits) {
        this.bbshits = bbshits;
    }
}
