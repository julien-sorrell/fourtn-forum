package com.fourtn.bbsadmin.bbsrelated;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/bbsadmin/bbsrelated")
public class BbsRelatedAPI {
    @Resource
    private BbsRelatedService bbsAdminService;

    @RequestMapping("/settop")
    public void settop(BbsRelatedDto dto, boolean bool) {
        bbsAdminService.settop(dto, bool);
    }

    @RequestMapping("/setgood")
    public void setgood(BbsRelatedDto dto, boolean bool) {
        bbsAdminService.setgood(dto, bool);
    }

    @RequestMapping("/setbbsremark")
    public void setbbsmark(BbsRelatedDto dto, String bbsmark) {
        bbsAdminService.setbbsmark(dto, bbsmark);
    }

}
