package com.fourtn.bbsadmin.bbsrelated;

public class BbsRelatedDto {
    private String bbsid;
    private String barid;
    private String userid;
    private String bbsmark;
    private String bbstitle;
    private Boolean bbstop;
    private Boolean bbsgood;
    private int bbshits;

    public String getBbsid() {
        return bbsid;
    }

    public void setBbsid(String bbsid) {
        this.bbsid = bbsid;
    }

    public String getBarid() {
        return barid;
    }

    public void setBarid(String barid) {
        this.barid = barid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBbsmark() {
        return bbsmark;
    }

    public void setBbsmark(String bbsmark) {
        this.bbsmark = bbsmark;
    }

    public String getBbstitle() {
        return bbstitle;
    }

    public void setBbstitle(String bbstitle) {
        this.bbstitle = bbstitle;
    }

    public Boolean getBbstop() {
        return bbstop;
    }

    public void setBbstop(Boolean bbstop) {
        this.bbstop = bbstop;
    }

    public Boolean getBbsgood() {
        return bbsgood;
    }

    public void setBbsgood(Boolean bbsgood) {
        this.bbsgood = bbsgood;
    }

    public int getBbshits() {
        return bbshits;
    }

    public void setBbshits(int bbshits) {
        this.bbshits = bbshits;
    }
}
