package com.fourtn.bbsadmin.bbsrelated;

import com.fourtn.bbsadmin.bbsrelated.BbsRelated;
import com.fourtn.bbsadmin.bbsrelated.BbsRelatedDto;

import java.util.List;

public interface BbsRelatedService {
    void settop(BbsRelatedDto dto, boolean bool);

    void setbbsmark(BbsRelatedDto dto, String bbsmark);

    void setgood(BbsRelatedDto dto, boolean bool);
}
