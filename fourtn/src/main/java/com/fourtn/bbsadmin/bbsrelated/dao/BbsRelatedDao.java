package com.fourtn.bbsadmin.bbsrelated.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;


@Mapper
public interface BbsRelatedDao {
    @Update("update bbs set bbstop = #{bool} where bbsid = #{bbsid}")
    void updatesettop(String bbsid, boolean bool);

    @Update("update bbs set bbsmark = #{bbsmark} where bbsid = #{bbsid}")
    void updatesetmark(String bbsid, String bbsmark);

    @Update("update bbs set bbsgood = #{bool} where bbsid = #{bbsid}")
    void updatesetgood(String bbsid, boolean bool);
}
