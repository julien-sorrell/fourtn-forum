package com.fourtn.bbsadmin.bbsrelated.impl;

import com.fourtn.bbsadmin.bbsrelated.BbsRelatedService;
import com.fourtn.bbsadmin.bbsrelated.BbsRelatedDto;
import com.fourtn.bbsadmin.bbsrelated.dao.BbsRelatedDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class BbsRelatedServiceImpl implements BbsRelatedService {
    @Resource
    private BbsRelatedDao bbsRelatedDao;

    @Override
    public void settop(BbsRelatedDto dto, boolean bool) {
        bbsRelatedDao.updatesettop(dto.getBbsid(), bool);
    }

    @Override
    public void setgood(BbsRelatedDto dto, boolean bool) {
        bbsRelatedDao.updatesetgood(dto.getBbsid(), bool);
    }

    @Override
    public void setbbsmark(BbsRelatedDto dto, String bbsmark) {
        bbsRelatedDao.updatesetmark(dto.getBbsid(), bbsmark);
    }
}
