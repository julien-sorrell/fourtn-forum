package com.fourtn.bbsadmin.checkreportrecord;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class CheckReportRecord {
    private String userreportid;
    private String reporterid;
    private String reportedid;
    private String reportmark;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String userreporttime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String userreporthandletime;
    private Boolean userisbanned;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String userbanendtime;
    private String userreportresult;

    public String getUserreportid() {
        return userreportid;
    }

    public void setUserreportid(String userreportid) {
        this.userreportid = userreportid;
    }

    public String getReporterid() {
        return reporterid;
    }

    public void setReporterid(String reporterid) {
        this.reporterid = reporterid;
    }

    public String getReportedid() {
        return reportedid;
    }

    public void setReportedid(String reportedid) {
        this.reportedid = reportedid;
    }

    public String getReportmark() {
        return reportmark;
    }

    public void setReportmark(String reportmark) {
        this.reportmark = reportmark;
    }



    public String getUserreportresult() {
        return userreportresult;
    }

    public void setUserreportresult(String userreportresult) {
        this.userreportresult = userreportresult;
    }

    public Boolean getUserisbanned() {
        return userisbanned;
    }

    public void setUserisbanned(Boolean userisbanned) {
        this.userisbanned = userisbanned;
    }

    public String getUserreporttime() {
        return userreporttime;
    }

    public void setUserreporttime(String userreporttime) {
        this.userreporttime = userreporttime;
    }

    public String getUserreporthandletime() {
        return userreporthandletime;
    }

    public void setUserreporthandletime(String userreporthandletime) {
        this.userreporthandletime = userreporthandletime;
    }

    public String getUserbanendtime() {
        return userbanendtime;
    }

    public void setUserbanendtime(String userbanendtime) {
        this.userbanendtime = userbanendtime;
    }
}
