package com.fourtn.bbsadmin.checkreportrecord;

import com.fourtn.bbsadmin.ban.Ban;
import com.fourtn.bbsadmin.ban.BanAPI;
import com.fourtn.bbsadmin.ban.BanService;
import com.fourtn.bbsadmin.ban.dao.BanDao;
import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/bbsadmin/checkreportrecord")
public class CheckReportRecordAPI {
    @Resource
    private CheckReportRecordService checkReportRecordService;
    @Resource
    private BanService banService;

    @RequestMapping("/getreportrecord")
    public Result getReportRecord(CheckReportRecordDto dto) {
        return Result.success(checkReportRecordService.getReportRecord(dto));
    }

    @PostMapping("/handle")
    public void handle(@RequestBody CheckReportRecord dto) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        dto.setUserreporthandletime(time);
        if(dto.getUserisbanned() != null)
            dto.setUserbanendtime(dto.getUserbanendtime().replace('T',' ').substring(0, 19));
        checkReportRecordService.updateReportRecord(dto);
        if(dto.getUserisbanned()){
            Ban ban = new Ban();
            ban.setBannedid(dto.getReportedid());
            ban.setBanreason(dto.getUserreportresult());
            ban.setEndtime(dto.getUserbanendtime());
            ban.setIsvalid(true);
            ban.setBantime(time);
            ban.setBanid(df.format(new Date())+ban.getBannedid()+ban.getBannerid());
            banService.insertBan(ban);
        }
    }
}
