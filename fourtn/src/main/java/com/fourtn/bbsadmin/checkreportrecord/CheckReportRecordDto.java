package com.fourtn.bbsadmin.checkreportrecord;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class CheckReportRecordDto {
    private String userreportid;
    private String reporterid;
    private String reportedid;
    private String reportmark;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String userreporttime_beg;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String userreporttime_end;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String userreporthandletime;
    private Boolean userisbanned;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String userbanendtime;
    private String userreportresult;

    public String getUserreportid() {
        return userreportid;
    }

    public void setUserreportid(String userreportid) {
        this.userreportid = userreportid;
    }

    public String getReporterid() {
        return reporterid;
    }

    public void setReporterid(String reporterid) {
        this.reporterid = reporterid;
    }

    public String getReportedid() {
        return reportedid;
    }

    public void setReportedid(String reportedid) {
        this.reportedid = reportedid;
    }

    public String getReportmark() {
        return reportmark;
    }

    public void setReportmark(String reportmark) {
        this.reportmark = reportmark;
    }



    public Boolean getUserisbanned() {
        return userisbanned;
    }

    public void setUserisbanned(Boolean userisbanned) {
        this.userisbanned = userisbanned;
    }


    public String getUserreportresult() {
        return userreportresult;
    }

    public void setUserreportresult(String userreportresult) {
        this.userreportresult = userreportresult;
    }

    public String getUserreporttime_beg() {
        return userreporttime_beg;
    }

    public void setUserreporttime_beg(String userreporttime_beg) {
        this.userreporttime_beg = userreporttime_beg;
    }

    public String getUserreporttime_end() {
        return userreporttime_end;
    }

    public void setUserreporttime_end(String userreporttime_end) {
        this.userreporttime_end = userreporttime_end;
    }

    public String getUserreporthandletime() {
        return userreporthandletime;
    }

    public void setUserreporthandletime(String userreporthandletime) {
        this.userreporthandletime = userreporthandletime;
    }

    public String getUserbanendtime() {
        return userbanendtime;
    }

    public void setUserbanendtime(String userbanendtime) {
        this.userbanendtime = userbanendtime;
    }
}
