package com.fourtn.bbsadmin.checkreportrecord;

import java.util.List;

public interface CheckReportRecordService {
    List<CheckReportRecord> getReportRecord(CheckReportRecordDto dto);

    void updateReportRecord(CheckReportRecord dto);
}
