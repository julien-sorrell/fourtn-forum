package com.fourtn.bbsadmin.checkreportrecord.dao;

import com.fourtn.bbsadmin.checkreportrecord.CheckReportRecord;
import com.fourtn.bbsadmin.checkreportrecord.CheckReportRecordDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface CheckReportRecordDao {
    List<CheckReportRecord> getReportRecord(CheckReportRecordDto dto);

    @Update("update reportrecord set userreporthandletime = #{userreporthandletime}, userisbanned = #{userisbanned}, userbanendtime = #{userbanendtime}, userreportresult = #{userreportresult} where userreportid = #{userreportid}")
    void updateReportRecord(CheckReportRecord dto);
}
