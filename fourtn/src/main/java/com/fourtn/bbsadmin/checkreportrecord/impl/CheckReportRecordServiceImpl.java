package com.fourtn.bbsadmin.checkreportrecord.impl;

import com.fourtn.bbsadmin.checkreportrecord.CheckReportRecord;
import com.fourtn.bbsadmin.checkreportrecord.CheckReportRecordDto;
import com.fourtn.bbsadmin.checkreportrecord.CheckReportRecordService;
import com.fourtn.bbsadmin.checkreportrecord.dao.CheckReportRecordDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class CheckReportRecordServiceImpl implements CheckReportRecordService {
    @Resource
    private CheckReportRecordDao checkReportRecordDao;

    @Override
    public List<CheckReportRecord> getReportRecord(CheckReportRecordDto dto) {
        return checkReportRecordDao.getReportRecord(dto);
    }

    @Override
    public void updateReportRecord(CheckReportRecord dto) {
        checkReportRecordDao.updateReportRecord(dto);
    }
}
