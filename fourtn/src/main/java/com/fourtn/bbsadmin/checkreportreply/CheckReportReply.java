package com.fourtn.bbsadmin.checkreportreply;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CheckReportReply {
    private String reportid;
    private String replyreporterid;
    private String replyid;
    private String reportmark;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String reporttime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String reporthandletime;
    private Boolean reportisdeleted;
    private String reportresult;

    public String getReportid() {
        return reportid;
    }

    public void setReportid(String reportid) {
        this.reportid = reportid;
    }

    public String getReplyreporterid() {
        return replyreporterid;
    }

    public void setReplyreporterid(String replyreporterid) {
        this.replyreporterid = replyreporterid;
    }

    public String getReplyid() {
        return replyid;
    }

    public void setReplyid(String replyid) {
        this.replyid = replyid;
    }

    public String getReportmark() {
        return reportmark;
    }

    public void setReportmark(String reportmark) {
        this.reportmark = reportmark;
    }

    public String getReporttime() {
        return reporttime;
    }

    public void setReporttime(String reporttime) {
        this.reporttime = reporttime;
    }

    public Boolean getReportisdeleted() {
        return reportisdeleted;
    }

    public void setReportisdeleted(Boolean reportisdeleted) {
        this.reportisdeleted = reportisdeleted;
    }

    public String getReportresult() {
        return reportresult;
    }

    public String getReporthandletime() {
        return reporthandletime;
    }

    public void setReporthandletime(String reporthandletime) {
        this.reporthandletime = reporthandletime;
    }

    public void setReportresult(String reportresult) {
        this.reportresult = reportresult;
    }

    @Override
    public String toString() {
        return "CheckReportReply{" +
                "reportid='" + reportid + '\'' +
                ", replyreporterid='" + replyreporterid + '\'' +
                ", replyid='" + replyid + '\'' +
                ", reportmark='" + reportmark + '\'' +
                ", reporttime='" + reporttime + '\'' +
                ", reporthandletime='" + reporthandletime + '\'' +
                ", reportisdeleted=" + reportisdeleted +
                ", reportresult='" + reportresult + '\'' +
                '}';
    }
}
