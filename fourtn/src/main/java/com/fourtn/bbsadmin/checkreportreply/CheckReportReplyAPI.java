package com.fourtn.bbsadmin.checkreportreply;

import com.fourtn.bbsadmin.delbbs.Delbbs;
import com.fourtn.bbsadmin.delreply.Delreply;
import com.fourtn.bbsadmin.delreply.DelreplyService;
import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/bbsadmin/checkreportreply")
public class CheckReportReplyAPI {
    @Resource
    private CheckReportReplyService checkReportReplyService;
    @Resource
    private DelreplyService delreplyService;

    @RequestMapping("/search")
    public Result search(CheckReportReplyDto dto) {
        return Result.success(checkReportReplyService.getReportReply(dto));
    }

    @PostMapping("/handle")
    public Result handle(@RequestBody CheckReportReply dto) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        dto.setReporthandletime(time);
        checkReportReplyService.updateReportReply(dto);
        if(dto.getReportisdeleted()) {
            if(checkReportReplyService.getFloor(dto) == 1) {
                Delbbs delbbs = new Delbbs();
                delbbs.setBbsid(checkReportReplyService.getBbsid(dto));
                checkReportReplyService.delete(dto);
                delreplyService.deleteBbs(delbbs);
            } else {
                Delreply delreply = new Delreply();
                delreply.setReplyid(dto.getReplyid());
                checkReportReplyService.delete(dto);
                delreplyService.deleteReply(delreply);
            }
        }
        return Result.success();
    }
}
