package com.fourtn.bbsadmin.checkreportreply;

import org.springframework.format.annotation.DateTimeFormat;

public class CheckReportReplyDto {
    private String reportid;
    private String replyreporterid;
    private String replyid;
    private String reportmark;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String reporttime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String reporthandletime_beg;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String reporthandletime_end;
    private Boolean reportisdeleted;
    private String reportresult;

    public String getReportid() {
        return reportid;
    }

    public void setReportid(String reportid) {
        this.reportid = reportid;
    }

    public String getReplyreporterid() {
        return replyreporterid;
    }

    public void setReplyreporterid(String replyreporterid) {
        this.replyreporterid = replyreporterid;
    }

    public String getReplyid() {
        return replyid;
    }

    public void setReplyid(String replyid) {
        this.replyid = replyid;
    }

    public String getReportmark() {
        return reportmark;
    }

    public void setReportmark(String reportmark) {
        this.reportmark = reportmark;
    }

    public String getReporttime() {
        return reporttime;
    }

    public void setReporttime(String reporttime) {
        this.reporttime = reporttime;
    }

    public Boolean getReportisdeleted() {
        return reportisdeleted;
    }

    public void setReportisdeleted(Boolean reportisdeleted) {
        this.reportisdeleted = reportisdeleted;
    }

    public String getReportresult() {
        return reportresult;
    }

    public void setReportresult(String reportresult) {
        this.reportresult = reportresult;
    }

    public String getReporthandletime_beg() {
        return reporthandletime_beg;
    }

    public void setReporthandletime_beg(String reporthandletime_beg) {
        this.reporthandletime_beg = reporthandletime_beg;
    }

    public String getReporthandletime_end() {
        return reporthandletime_end;
    }

    public void setReporthandletime_end(String reporthandletime_end) {
        this.reporthandletime_end = reporthandletime_end;
    }
}
