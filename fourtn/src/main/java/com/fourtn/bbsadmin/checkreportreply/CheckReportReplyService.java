package com.fourtn.bbsadmin.checkreportreply;

import java.util.List;

public interface CheckReportReplyService {
    //List<CheckReportReply> getReportRecord(CheckReportReplyDto dto);

    void updateReportReply(CheckReportReply dto);

    int getFloor(CheckReportReply dto);

    String getBbsid(CheckReportReply dto);
    
    void delete(CheckReportReply dto);

    List<CheckReportReply> getReportReply(CheckReportReplyDto dto);
}
