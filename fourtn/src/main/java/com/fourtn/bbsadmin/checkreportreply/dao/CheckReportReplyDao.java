package com.fourtn.bbsadmin.checkreportreply.dao;

import com.fourtn.bbsadmin.checkreportreply.CheckReportReply;
import com.fourtn.bbsadmin.checkreportreply.CheckReportReplyDto;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface CheckReportReplyDao {
    List<CheckReportReply> getReportReply(CheckReportReplyDto dto);

    @Update("update reportreply set reporthandletime = #{reporthandletime}, reportisdeleted = #{reportisdeleted}, reportresult = #{reportresult} where reportid = #{reportid}")
    void updateReportReply(CheckReportReply dto);

    @Select("select replyfloor from reply, reportreply where reportreply.replyid = reply.replyid and reply.replyid = #{replyid}")
    List<Integer> getFloor(CheckReportReply dto);

    @Select("select bbsid from reply, reportreply where reportreply.replyid = reply.replyid and reply.replyid = #{replyid}")
    List<String> getBbsid(CheckReportReply dto);

    @Delete("delete from reportreply where replyid = #{replyid}")
    void delete(CheckReportReply dto);
}
