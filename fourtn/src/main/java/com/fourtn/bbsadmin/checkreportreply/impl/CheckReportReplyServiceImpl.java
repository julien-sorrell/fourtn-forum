package com.fourtn.bbsadmin.checkreportreply.impl;

import com.fourtn.bbsadmin.checkreportreply.CheckReportReply;
import com.fourtn.bbsadmin.checkreportreply.CheckReportReplyDto;
import com.fourtn.bbsadmin.checkreportreply.CheckReportReplyService;
import com.fourtn.bbsadmin.checkreportreply.dao.CheckReportReplyDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class CheckReportReplyServiceImpl implements CheckReportReplyService {
    @Resource
    private CheckReportReplyDao checkReportReplyDao;

    @Override
    public void updateReportReply(CheckReportReply dto) {
        checkReportReplyDao.updateReportReply(dto);
    }

    @Override
    public int getFloor(CheckReportReply dto) {
        return checkReportReplyDao.getFloor(dto).get(0);
    }

    @Override
    public String getBbsid(CheckReportReply dto) {
        return checkReportReplyDao.getBbsid(dto).get(0);
    }

    @Override
    public void delete(CheckReportReply dto) {
        checkReportReplyDao.delete(dto);
    }

    @Override
    public List<CheckReportReply> getReportReply(CheckReportReplyDto dto) {
        List<CheckReportReply> h = checkReportReplyDao.getReportReply(dto);
        return checkReportReplyDao.getReportReply(dto);
    }
}
