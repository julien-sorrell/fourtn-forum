package com.fourtn.bbsadmin.delbbs;

import com.fourtn.bbsadmin.delreply.DelreplyService;
import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/bbsadmin/delbbs")
public class DelbbsAPI {
    @Resource
    private DelreplyService delreplyService;

    @RequestMapping("/delete")
    public Result delete(Delbbs delbbs) {
        delreplyService.deleteBbs(delbbs);
        return Result.success();
    }
}
