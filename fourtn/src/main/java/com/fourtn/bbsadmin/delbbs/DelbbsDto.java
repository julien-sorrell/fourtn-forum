package com.fourtn.bbsadmin.delbbs;

public class DelbbsDto {
    private String bbsid;

    public String getBbsid() {
        return bbsid;
    }

    public void setBbsid(String bbsid) {
        this.bbsid = bbsid;
    }
}
