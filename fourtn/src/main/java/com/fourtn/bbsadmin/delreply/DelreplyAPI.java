package com.fourtn.bbsadmin.delreply;

import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/bbsadmin/delreply")
public class DelreplyAPI {
    @Resource
    private DelreplyService delreplyService;

    @RequestMapping("/delete")
    public Result delete(Delreply delreply) {
        delreplyService.deleteReply(delreply);
        return Result.success();
    }
}
