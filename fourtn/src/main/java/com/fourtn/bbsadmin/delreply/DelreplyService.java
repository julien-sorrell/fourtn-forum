package com.fourtn.bbsadmin.delreply;

import com.fourtn.bbsadmin.delbbs.Delbbs;

public interface DelreplyService {

    void deleteReply(Delreply delreply);

    void deleteBbs(Delbbs delbbs);
}
