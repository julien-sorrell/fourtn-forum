package com.fourtn.bbsadmin.delreply.dao;

import com.fourtn.bbsadmin.delbbs.Delbbs;
import com.fourtn.bbsadmin.delreply.Delreply;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DelreplyDao {
    @Delete("delete from reply where replyid = #{replyid}")
    void deleteReply(Delreply delreply);

    @Delete("delete from reportreply where replyid = #{replyid}")
    void deleteReply2(Delreply delreply);

    @Delete("delete from reply where bbsid = #{bbsid}")
    void deleteAllReply(Delbbs delbbs);

    @Delete("delete from bbs where bbsid = #{bbsid}")
    void deleteBbs(Delbbs delbbs);
}
