package com.fourtn.bbsadmin.delreply.impl;

import com.fourtn.bbsadmin.delbbs.Delbbs;
import com.fourtn.bbsadmin.delreply.Delreply;
import com.fourtn.bbsadmin.delreply.DelreplyService;
import com.fourtn.bbsadmin.delreply.dao.DelreplyDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class DelreplyServiceImpl implements DelreplyService {
    @Resource
    private DelreplyDao delreplyDao;

    @Override
    public void deleteReply(Delreply delreply) {
        delreplyDao.deleteReply2(delreply);
        delreplyDao.deleteReply(delreply);
    }

    @Override
    public void deleteBbs(Delbbs delbbs) {
        delreplyDao.deleteAllReply(delbbs);
        delreplyDao.deleteBbs(delbbs);
    }
}
