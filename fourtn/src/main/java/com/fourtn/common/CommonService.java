package com.fourtn.common;


import com.fourtn.common.model.User.User;

public interface CommonService {
    User getUserById(String userId);
}
