package com.fourtn.common.base;

import com.fourtn.common.Result;
import com.fourtn.common.Utils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public void uploadException(MaxUploadSizeExceededException e, HttpServletResponse response) throws IOException {
        Utils.outJson(response, Result.fail(Result.ERR_CODE_BUSINESS,"文件大小超出限制！"));
    }
}