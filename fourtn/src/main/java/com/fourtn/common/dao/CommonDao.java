package com.fourtn.common.dao;

import com.fourtn.common.model.User.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


public interface CommonDao {

    @Select("select userid userId,username userName,useravatar avatar,password userPwd  from user where userid = #{userId}")
    User findUserById(String userId);
}
