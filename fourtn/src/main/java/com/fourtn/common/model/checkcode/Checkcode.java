package com.fourtn.common.model.checkcode;

import java.util.Date;


public class Checkcode {

    private Integer id;
    private String skey;
    private Integer code;
    private Date checktime;

    public Checkcode() {
    }

    public Checkcode(Integer id, String skey, Integer code, Date checktime) {
        this.id = id;
        this.skey = skey;
        this.code = code;
        this.checktime = checktime;
    }

    public Checkcode(String key, Integer code, Date checktime) {
        this.skey = key;
        this.code = code;
        this.checktime = checktime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return skey;
    }

    public void setKey(String key) {
        this.skey = key;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Date getChecktime() {
        return checktime;
    }

    public void setChecktime(Date checktime) {
        this.checktime = checktime;
    }
}
