package com.fourtn.info.user.dao;

import com.fourtn.info.user.follow.IntoFollow;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface FollowDao {
    @Insert("insert into followship values(#{follower},#{userid})")
    void insertIntoFollow(IntoFollow intoFollow);

    @Delete("delete from followship where follower = #{follower} and use_userid = #{userid}")
    void deleteIntoFollow(IntoFollow intoFollow);

    @Select("select count(*) from followship where follower = #{follower} and use_userid = #{userid}")
    int findfollowcount(IntoFollow intoFollow);
}
