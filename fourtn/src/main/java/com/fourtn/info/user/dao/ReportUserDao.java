package com.fourtn.info.user.dao;

import com.fourtn.info.user.reportUser.IntoReportUser;
import com.fourtn.info.user.reportUser.Reportmark;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ReportUserDao {
    @Insert("insert into reportrecord (userreportid,reporterid,reportedid,reportmark,userreporttime) values(#{userreportid},#{reporterid},#{reportedid},#{reportmark},#{userreporttime})")
    void insertIntoReportRecord(IntoReportUser intoReportUser);

    @Select("select reportmark from reportmark")
    List<Reportmark> getReportmark();

    @Select("select count(*) from reportrecord where reporterid = #{reporterid} and reportedid = #{userid}")
    int findreportcount(String reporterid, String userid);
}
