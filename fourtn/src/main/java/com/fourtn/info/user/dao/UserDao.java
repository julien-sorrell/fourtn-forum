package com.fourtn.info.user.dao;

import com.fourtn.info.user.user.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;


public interface UserDao {
    List<CollectbbsVO> findCollectbbsVOByUserid(String userId);

    List<BarVO> findBarVOByUserid(String userId);

    List<BbsVO> findBbsVOByUserid(String userId);

    List<ReplyVO> findReplyVOByUserid(String userId);

    List<FollowerVO> findFollowerVOByUserid(String userId);

    List<FollowerVO> findAttentionVOByUserid(String userId);

    @Select("select count(use_userid) from followship where follower = #{userid}")
    int findCountByfollower(String userid);
    @Select("select count(follower) from followship where use_userid = #{userid}")
    int findCountByattention(String userid);

    List<ReplyVO> findOtherReplyVOByUserid(String userId);

    @Update("update user set useravatar = #{useravatar} where userid = #{userid}")
    void updateAvatar(@Param("userid") String userid, @Param("useravatar") String filename);

    /**
     * 依照一个账号查询一个用户信息
     * @return
     */
    User findUser(String userid);

    /**
     * 更新用户信息
     * @param dto
     */
    void updateUser(UserDto dto);
}
