package com.fourtn.info.user.follow;

import com.fourtn.common.CommonService;
import com.fourtn.common.CurrentUser;
import com.fourtn.common.Result;
import com.fourtn.common.TokenUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/info/user/follow")
public class FollowAPI {
    @Resource private FollowService followService;
    @Resource private CommonService commonService;
    @PostMapping("/insert")//接收要关注的人,参数名为userid
    public Result insertIntoFollow(@RequestHeader("Token") String token,@RequestBody IntoFollow intoFollow){
        CurrentUser currentUser = TokenUtils.getUserInfo(token, commonService);
        followService.insertIntoFollow(intoFollow,currentUser);
        return Result.success("关注成功");
    }
    @PostMapping("/delete")//接收要取消关注的人,参数名为userid
    public Result deleteIntoFollow(@RequestHeader("Token") String token,@RequestBody IntoFollow intoFollow){
        CurrentUser currentUser = TokenUtils.getUserInfo(token, commonService);
        followService.deleteIntoFollow(intoFollow,currentUser);
        return Result.success("取消关注成功");
    }
    @PostMapping("/select")
    public Result findfollowcount(@RequestHeader("Token") String token,@RequestBody IntoFollow intoFollow){
        CurrentUser currentUser = TokenUtils.getUserInfo(token, commonService);
        return Result.success(followService.findfollowcount(intoFollow,currentUser));
    }
}
