package com.fourtn.info.user.follow;

import com.fourtn.common.CurrentUser;
import com.fourtn.info.user.dao.FollowDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class FollowService {
    @Resource
    private FollowDao followDao;

    public void insertIntoFollow(IntoFollow intoFollow, CurrentUser currentUser) {
        intoFollow.setFollower(currentUser.getUserId());
        followDao.insertIntoFollow(intoFollow);
    }

    public void deleteIntoFollow(IntoFollow intoFollow, CurrentUser currentUser) {
        intoFollow.setFollower(currentUser.getUserId());
        followDao.deleteIntoFollow(intoFollow);
    }

    public boolean findfollowcount(IntoFollow intoFollow, CurrentUser currentUser) {
        intoFollow.setFollower(currentUser.getUserId());
        return followDao.findfollowcount(intoFollow)==1;
    }
}
