package com.fourtn.info.user.follow;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class IntoFollow {
    private String userid;
    private String follower;
}
