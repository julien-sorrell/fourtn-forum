package com.fourtn.info.user.reportUser;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
@Getter
@Setter
@ToString
public class IntoReportUser {
    private String userreportid;
    private String reporterid;
    private String reportedid;
    private String reportmark;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    private Date userreporttime;
}
