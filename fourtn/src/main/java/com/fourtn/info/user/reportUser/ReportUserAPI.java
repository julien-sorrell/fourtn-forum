package com.fourtn.info.user.reportUser;

import com.fourtn.common.CommonService;
import com.fourtn.common.CurrentUser;
import com.fourtn.common.Result;
import com.fourtn.common.TokenUtils;
import com.fourtn.info.user.follow.IntoFollow;
import org.apache.ibatis.annotations.Insert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/info/user/report")
public class ReportUserAPI {
    @Resource private ReportUserService reportUserService;
    @Resource private CommonService commonService;
    @PostMapping("/insert")//接收reportmark、reportedid
    public Result insertIntoReportUser(@RequestHeader("Token") String token, @RequestBody IntoReportUser intoReportUser){
        CurrentUser currentUser = TokenUtils.getUserInfo(token,commonService);
        reportUserService.insertIntoReportUser(currentUser,intoReportUser);
        return Result.success("举报成功");
    }
    @GetMapping("/reportmark")
    public Result getReportmark(){
        List<Reportmark> reportmarkList = reportUserService.getReportmark();
        return Result.success(reportmarkList);
    }
    @PostMapping("/select/{userid}")
    public Result findreportcount(@RequestHeader("Token") String token,@PathVariable String userid){
        CurrentUser currentUser = TokenUtils.getUserInfo(token, commonService);
        return Result.success(reportUserService.findreportcount(userid,currentUser));
    }
}
