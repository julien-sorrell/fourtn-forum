package com.fourtn.info.user.reportUser;

import com.fourtn.common.CurrentUser;
import com.fourtn.info.user.dao.ReportUserDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
@Transactional
public class ReportUserService {

    @Resource private ReportUserDao reportUserDao;
    public void insertIntoReportUser(CurrentUser currentUser, IntoReportUser intoReportUser) {
        intoReportUser.setReporterid(currentUser.getUserId());
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        intoReportUser.setUserreporttime(new Date());
        String userreportid = sdf.format(now);
        intoReportUser.setUserreportid(userreportid);
        reportUserDao.insertIntoReportRecord(intoReportUser);
    }

    public List<Reportmark> getReportmark() {
        return reportUserDao.getReportmark();
    }

    public boolean findreportcount(String userid, CurrentUser currentUser) {
        String reporterid = currentUser.getUserId();
        return reportUserDao.findreportcount(reporterid,userid)==1;
    }
}
