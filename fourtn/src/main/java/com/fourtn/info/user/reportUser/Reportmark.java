package com.fourtn.info.user.reportUser;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Reportmark {
    String reportmark;
}
