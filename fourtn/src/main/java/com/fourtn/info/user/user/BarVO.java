package com.fourtn.info.user.user;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class BarVO {
    private String  barname;
    private String  baravatar;
    private String  bardescription;
    private String barid;

    public String getBarname() {
        return barname;
    }

    public void setBarname(String barname) {
        this.barname = barname;
    }

    public String getBaravatar() {
        return baravatar;
    }

    public void setBaravatar(String baravatar) {
        this.baravatar = baravatar;
    }

    public String getBardescription() {
        return bardescription;
    }

    public void setBardescription(String bardescription) {
        this.bardescription = bardescription;
    }
}
