package com.fourtn.info.user.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class BbsVO {
    private String barname;
    private String bbstitle;
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:SS")
    private Date bbstime;
    private String bbsid;

    public String getBarname() {
        return barname;
    }

    public void setBarname(String barname) {
        this.barname = barname;
    }

    public String getBbstitle() {
        return bbstitle;
    }

    public void setBbstitle(String bbstitle) {
        this.bbstitle = bbstitle;
    }

    public Date getBbstime() {
        return bbstime;
    }

    public void setBbstime(Date bbstime) {
        this.bbstime = bbstime;
    }
}
