package com.fourtn.info.user.user;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CollectbbsVO {
    private String username;
    private String useravatar;
    private String bbstitle;
    private String bbsid;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUseravatar() {
        return useravatar;
    }

    public void setUseravatar(String useravatar) {
        this.useravatar = useravatar;
    }

    public String getBbstitle() {
        return bbstitle;
    }

    public void setBbstitle(String bbstitle) {
        this.bbstitle = bbstitle;
    }
}
