package com.fourtn.info.user.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FollowerVO {
    private String username;
    private String useravatar;
    private String usersignature;
    private String userid;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUseravatar() {
        return useravatar;
    }

    public void setUseravatar(String useravatar) {
        this.useravatar = useravatar;
    }

    public String getUsersignature() {
        return usersignature;
    }

    public void setUsersignature(String usersignature) {
        this.usersignature = usersignature;
    }
}
