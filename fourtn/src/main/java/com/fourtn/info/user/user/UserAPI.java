package com.fourtn.info.user.user;


import com.fourtn.common.CommonService;
import com.fourtn.common.CurrentUser;
import com.fourtn.common.Result;
import com.fourtn.common.TokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/info/user")
public class UserAPI {
    @Resource
    private UserService userService;
    @Resource
    private CommonService commonService;

    @GetMapping("/collectbbs/{userid}")
    public Result collectbbsList(@PathVariable String userid) {
        List<CollectbbsVO> collectbbsVOList = userService.getCollectbbsVO(userid);
        return Result.success(collectbbsVOList);
    }

    @GetMapping("/bar/{userid}")
    public Result barList(@PathVariable String userid) {
        List<BarVO> barVOList = userService.getBarVO(userid);
        return Result.success(barVOList);
    }

    @GetMapping("/bbs/{userid}")
    public Result bbsList(@PathVariable String userid) {
        List<BbsVO> bbsVOList = userService.getBbsVO(userid);
        return Result.success(bbsVOList);
    }

    @GetMapping("/reply/{userid}")
    public Result replyList(@PathVariable String userid) {
        List<ReplyVO> replyVOList = userService.getReplyVO(userid);
        return Result.success(replyVOList);
    }

    @GetMapping("/follower/{userid}")
    public Result followerList(@PathVariable String userid) {
        List<FollowerVO> followerVOList = userService.getFollowerVO(userid);
        return Result.success(followerVOList);
    }

    @GetMapping("/attention/{userid}")
    public Result attentionList(@PathVariable String userid) {
        List<FollowerVO> attentionVOList = userService.getAttentionVO(userid);
        return Result.success(attentionVOList);
    }

    @GetMapping("/followerCount/{userid}")//关注量
    public Result followerCount(@PathVariable String userid) {
        int followerCount = userService.getfollowerCount(userid);
        return Result.success(followerCount);
    }

    @GetMapping("/attentionCount/{userid}")//粉丝量
    public Result attentionCount(@PathVariable String userid) {
        int attentionCount = userService.getattentionCount(userid);
        return Result.success(attentionCount);
    }

    @GetMapping("/otherReply/{userid}")
    public Result otherreplyList(@PathVariable String userid) {
        List<ReplyVO> otherReplyVOList = userService.getOtherReplyVO(userid);
        return Result.success(otherReplyVOList);
    }

    /**
     * 下载文件
     *
     * @param filename
     * @param response
     */
    @GetMapping("/avatar/{filename}")
    public void getAvator(@PathVariable String filename, HttpServletResponse response) throws IOException {
        if (filename.equals("null") || filename == null) return;

        //读取文件的输入流
        InputStream in = new FileInputStream(User.AVATAR_DIR + "/" + filename);
        //将文件信息输出到浏览器的输出流
        OutputStream out = response.getOutputStream();

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
        out.flush();
        out.close();
        in.close();
    }

    public void init() throws ServletException {
        File dir = new File(User.AVATAR_DIR);//表示文件或目录的路径
        if (!dir.exists()) {//目录不存在则创建
            dir.mkdirs();

        }
    }

    @PostMapping("/avatar/{userid}")
    public Result uploadAvaor(MultipartFile avatar, @PathVariable String userid) throws IOException, ServletException {

        init();

        InputStream in = avatar.getInputStream();

        String name = UUID.randomUUID().toString();//获得通过uuid算法得到的不重复的字符串
        String submitedFilename = avatar.getOriginalFilename();//原始文件名
        String suffix = submitedFilename.substring(submitedFilename.lastIndexOf("."));
        String filename = name + suffix;
        OutputStream out = new FileOutputStream(User.AVATAR_DIR + "/" + filename);

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
        out.flush();
        out.close();
        in.close();

        //CurrentUser currentUser = TokenUtils.getUserInfo(token,commonService);
        userService.setAvatar(userid, filename);

        return Result.success();
    }

    @GetMapping("/{userid}")
    public Result queryUserById(@PathVariable String userid) {
        User user = userService.getUser(userid);
        return Result.success(user);
    }

    /**
     * 修改用户信息
     *
     * @param dto
     * @return
     */
    @PutMapping("")
    public Result updateUser(@RequestBody UserDto dto) {
        userService.updateUser(dto);
        return Result.success();
    }

    @PostMapping("/select/{userid}")
    public Result findusercount(@RequestHeader("Token") String token, @PathVariable String userid) {
        CurrentUser currentUser = TokenUtils.getUserInfo(token, commonService);
        return Result.success(userService.findusercount(userid, currentUser));
    }
}
