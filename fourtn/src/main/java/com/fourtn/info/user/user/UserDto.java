package com.fourtn.info.user.user;


import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class UserDto {
    private String userid;
    private String username;
    private String usersex;
    private String password;
    private String usermail;
    private String usersignature;

    public String getUsersignature() {
        return usersignature;
    }

    public void setUsersignature(String usersignature) {
        this.usersignature = usersignature;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsermail() {
        return usermail;
    }

    public void setUsermail(String usermail) {
        this.usermail = usermail;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:SS")
    private Date registertime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date registertime_start;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date registertime_end;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsersex() {
        return usersex;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex;
    }

    public Date getRegistertime() {
        return registertime;
    }

    public void setRegistertime(Date registertime) {
        this.registertime = registertime;
    }

    public Date getRegistertime_start() {
        return registertime_start;
    }

    public void setRegistertime_start(Date registertime_start) {
        this.registertime_start = registertime_start;
    }

    public Date getRegistertime_end() {
        return registertime_end;
    }

    public void setRegistertime_end(Date registertime_end) {
        this.registertime_end = registertime_end;
    }
}
