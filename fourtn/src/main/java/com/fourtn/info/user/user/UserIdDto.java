package com.fourtn.info.user.user;

import com.fourtn.common.PageParam;

public class UserIdDto {
    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
