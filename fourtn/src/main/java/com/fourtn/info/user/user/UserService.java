package com.fourtn.info.user.user;
import com.fourtn.common.CurrentUser;

import java.util.List;

public interface UserService {
    boolean findusercount(String userid, CurrentUser currentUser);

    List<CollectbbsVO> getCollectbbsVO(String userid);

    List<BarVO> getBarVO(String userid);

    List<BbsVO> getBbsVO(String userid);

    List<ReplyVO> getReplyVO(String userid);

    List<FollowerVO> getFollowerVO(String userid);

    List<FollowerVO> getAttentionVO(String userid);

    int getfollowerCount(String userid);
    int getattentionCount(String userid);

    List<ReplyVO> getOtherReplyVO(String userid);

    void setAvatar(String userid, String filename);

    User getUser(String userid);

    void updateUser(UserDto dto);
}
