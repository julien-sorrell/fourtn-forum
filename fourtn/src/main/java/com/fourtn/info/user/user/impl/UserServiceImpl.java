package com.fourtn.info.user.user.impl;
import com.fourtn.common.CurrentUser;
import com.fourtn.info.user.dao.UserDao;
import com.fourtn.info.user.user.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public boolean findusercount(String userid, CurrentUser currentUser) {
        String id = currentUser.getUserId();
        return id.equals(userid);
    }

    @Override
    public List<CollectbbsVO> getCollectbbsVO(String userid) {
        return userDao.findCollectbbsVOByUserid(userid);
    }

    @Override
    public List<BarVO> getBarVO(String userid) {
        return userDao.findBarVOByUserid(userid);
    }


    @Override
    public List<BbsVO> getBbsVO(String userid) {
        return userDao.findBbsVOByUserid(userid);
    }

    @Override
    public List<ReplyVO> getReplyVO(String userid) {
        return userDao.findReplyVOByUserid(userid);
    }

    @Override
    public List<FollowerVO> getFollowerVO(String userid) {
        return userDao.findFollowerVOByUserid(userid);
    }

    @Override
    public List<FollowerVO> getAttentionVO(String userid) {
        return userDao.findAttentionVOByUserid(userid);
    }

    @Override
    public int getfollowerCount(String userid) {
        return userDao.findCountByfollower(userid);
    }

    @Override
    public int getattentionCount(String userid) {
        return userDao.findCountByattention(userid);
    }

    @Override
    public List<ReplyVO> getOtherReplyVO(String userid) {
        return userDao.findOtherReplyVOByUserid(userid);
    }

    @Override
    public void setAvatar(String userid, String filename) {
        userDao.updateAvatar(userid,filename);
    }

    @Override
    public User getUser(String userid) {
        return userDao.findUser(userid);
    }

    @Override
    public void updateUser(UserDto dto) {
        userDao.updateUser(dto);
    }
}
