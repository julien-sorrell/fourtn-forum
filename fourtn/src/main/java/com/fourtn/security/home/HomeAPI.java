package com.fourtn.security.home;

import com.fourtn.common.CommonService;
import com.fourtn.common.CurrentUser;
import com.fourtn.common.Result;
import com.fourtn.common.TokenUtils;
import com.fourtn.info.user.user.User;
import com.fourtn.info.user.user.UserService;
import com.fourtn.userfunction.bar.BarDto;
import com.fourtn.userfunction.bar.BarService;

import com.fourtn.userfunction.bbs.BbsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;



import com.fourtn.security.login.LoginDto;
import com.fourtn.userfunction.bar.BarService;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;



@RestController
@RequestMapping("/security/home")
public class HomeAPI {
    @Resource
    CommonService commonService;
    @Resource
    private UserService userService;



    @Resource
    private HomeService homeService;

    @RequestMapping("/user-list")
    public Result userList(){
        return Result.success(homeService.getUserList());
    }
    @RequestMapping("/bar-list")
    public Result barList(){
        return Result.success(homeService.getBarList());
    }

    @RequestMapping("/bbs-list")
    public Result bbsList(){
        return Result.success(homeService.getBbslist());
    }

    @PostMapping("/searchuser")
    public Result search(@RequestBody  SearchDto searchdto){
        System.out.println("searchdto"+searchdto);
        return Result.success(homeService.searchuser(searchdto));
    }


    @PostMapping("/searchuserbyid")
    public Result searchuserid(@RequestBody  SearchDto searchdto){

        return Result.success(homeService.searchuserbyid(searchdto));
    }

    @PostMapping("/searchbar")
    public Result searchbar(@RequestBody SearchDto searchdto){
        return Result.success(homeService.searchbar(searchdto));
    }
    @PostMapping("/searchbbs")
    public Result searchbbs(@RequestBody SearchDto searchdto){
        return Result.success(homeService.searchbbs(searchdto));}

//    @PostMapping("/searchuserbyid")
//    public Result searchuserid(@RequestBody  SearchDto searchdto){
//
//        return Result.success(homeService.searchuserbyid(searchdto));
//    }



    @GetMapping("/curr-user")
    public Result getCurrUser(@RequestHeader("Token") String token){
        CurrentUser currentUser = TokenUtils.getUserInfo(token,commonService);
        return Result.success(currentUser);
    }
    @DeleteMapping("/exit")
    public Result exit(@RequestHeader("Token") String token){
        if(token == null || token.equals("")) {return Result.success(new CurrentUser());}
        //在服务端清除缓存的token
        TokenUtils.removeToken(token);
        return Result.success();
    }

    @RequestMapping("/follownum")
    public Result follownum(){

        return Result.success(homeService.follownum());
    }

    @RequestMapping("/type")
    public Result gettype(){
        return Result.success(homeService.gettype());
    }


    @GetMapping("/avatar/{filename}")
    public void getAvator(@PathVariable String filename, HttpServletResponse response) throws IOException {

        //读取文件的输入流
        InputStream in = new FileInputStream(User.AVATAR_DIR+"/"+filename);
        //将文件信息输出到浏览器的输出流
        OutputStream out = response.getOutputStream();

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while((len=in.read(b))!=-1){
            out.write(b,0,len);
        }
        out.flush();
        out.close();
        in.close();
    }











}