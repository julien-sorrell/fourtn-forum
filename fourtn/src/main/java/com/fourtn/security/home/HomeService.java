package com.fourtn.security.home;




import com.fourtn.info.user.user.User;
import com.fourtn.security.login.LoginDto;
import com.fourtn.userfunction.bar.Bar;
import com.fourtn.userfunction.bbs.Bbs;




import java.util.List;


public interface HomeService {

    User findcurrentuser(LoginDto dto);
    User searchuserbyid(SearchDto dto);
    List<User> searchuser(SearchDto dto);
    List<Bar> searchbar(SearchDto dto);

    List<Bbs> getBbslist();
    List<follow> follownum();
    List<Bbs> searchbbs(SearchDto searchdto);

    List<String> gettype();

    List<Bar> getBarList();

    List<User> getUserList();
}
