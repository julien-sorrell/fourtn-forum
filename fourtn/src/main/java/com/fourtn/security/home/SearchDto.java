package com.fourtn.security.home;

public class SearchDto {

    private String searchcontent;

    public String getSearchcontent() {
        return searchcontent;
    }

    public void setSearchcontent(String searchcontent) {
        this.searchcontent = searchcontent;
    }

}
