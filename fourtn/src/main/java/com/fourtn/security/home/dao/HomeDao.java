package com.fourtn.security.home.dao;



import com.fourtn.info.user.user.User;
import com.fourtn.security.home.SearchDto;
import com.fourtn.security.home.follow;
import com.fourtn.security.login.LoginDto;
import com.fourtn.userfunction.bar.Bar;



import com.fourtn.userfunction.bbs.Bbs;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface HomeDao {


    @Select("select follow.barid,barname,bardescription,bar.userid,baravatar,barhits,bartime,barmark,count(*) follownum from bar,follow where bar.barid=follow.barid GROUP BY bar.barid order by barhits desc")
    List<Bar> findBarList();
    @Select("select bbs.bbsid,bbs.barid,bbs.userid,bbsmark,bbstitle,bbstime,bbslike,bbsunlike,bbstop,bbsgood,bbshits,barname,username,content from bbs,bar,user,reply \n" +
            "where  bbs.barid=bar.barid and bar.userid=user.userid and bbs.bbsid=reply.bbsid order by 0.3*bbshits+0.7*bbslike desc")
    List<Bbs> findBbslist();



      @Select("select * from user where userid=#{userid} and password =#{password} ")
      User findcurrentuser(LoginDto dto);

      @Select("select * from user where userid=#{searchcontent}")
      User searchuserbyid(SearchDto dto);

      @Select("select * from user where username like concat('%',#{searchcontent},'%')")
      List<User> searchuser(SearchDto dto);

      @Select("select * from bar where barname like concat('%',#{searchcontent},'%')")
      List<Bar> searchbar(SearchDto dto);

    @Select("select follow.barid,count(*) follownum from follow GROUP BY barid ")
    List<follow> getfollownum();
    @Select("select bbsid,bbs.barid,bbs.userid,bbsmark,bbstitle,bbstime,bbslike,bbsunlike,bbstop,bbsgood,bbshits,barname,username from bbs,bar,user where bbstitle like concat('%',#{searchcontent},'%') and bbs.barid=bar.barid and bar.userid=user.userid")
    List<Bbs> searchbbs(SearchDto searchdto);
    @Select("select * from bbsmark")
    List<String> findtypelist();
    @Select(("select * from user"))
    List<User> getUserList();
}
