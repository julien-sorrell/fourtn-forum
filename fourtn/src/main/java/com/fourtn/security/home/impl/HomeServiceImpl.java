package com.fourtn.security.home.impl;




import com.fourtn.info.user.user.User;
import com.fourtn.security.home.HomeService;
import com.fourtn.security.home.SearchDto;
import com.fourtn.security.home.dao.HomeDao;
import com.fourtn.security.home.follow;
import com.fourtn.security.login.LoginDto;
import com.fourtn.userfunction.bar.Bar;



import com.fourtn.userfunction.bbs.Bbs;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class HomeServiceImpl implements HomeService {

    @Resource
    private HomeDao homeDao;
    @Override
    public User findcurrentuser(LoginDto dto) {
        return homeDao.findcurrentuser(dto);

    }

    @Override
    public User searchuserbyid(SearchDto dto) {
        return homeDao.searchuserbyid(dto);
    }

    @Override
    public List<User> searchuser(SearchDto dto) {
        return homeDao.searchuser(dto);
    }

    @Override
    public List<Bar> searchbar(SearchDto dto) {
        return homeDao.searchbar(dto);
    }


    @Override
    public List<Bbs> getBbslist() {
        return homeDao.findBbslist();
    }

    @Override
    public List<follow> follownum() {
        return homeDao.getfollownum();
    }

    @Override
    public List<Bbs> searchbbs(SearchDto searchdto) {
        return homeDao.searchbbs(searchdto);
    }

    @Override
    public List<String> gettype() {
        return homeDao.findtypelist();
    }

    @Override
    public List<Bar> getBarList() {
        return homeDao.findBarList();
    }

    @Override
    public List<User> getUserList() {
        return homeDao.getUserList();
    }


}
