package com.fourtn.security.login;

import com.fourtn.common.Result;
import com.fourtn.common.TokenUtils;



import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/security/login")
public class LoginAPI {

    @Resource
    private LoginService loginService;

    @PostMapping("")
    public Result login(@RequestBody LoginDto dto){

        boolean ok = loginService.checkLogin(dto);

        if(ok){
            //生成一个登录令牌
            String token = TokenUtils.loginSign(dto.getUserid(),dto.getPassword());
            return Result.success((Object)token);//想客户端发送成功信号，并发送令牌

        }

        return Result.fail(Result.ERR_CODE_BUSINESS, "账号或密码错误！");

    }
}
