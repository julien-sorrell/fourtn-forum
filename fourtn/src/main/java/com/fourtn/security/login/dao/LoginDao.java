package com.fourtn.security.login.dao;


import com.fourtn.security.login.LoginDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface LoginDao {

    @Select("select count(userid) from user where userid=#{userid} and password =#{password} ")
    int findUserCountByIdAndPwd(LoginDto dto);
}
