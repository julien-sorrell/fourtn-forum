package com.fourtn.security.login.impl;




import com.fourtn.info.user.dao.UserDao;
import com.fourtn.security.login.LoginDto;
import com.fourtn.security.login.LoginService;
import com.fourtn.security.login.dao.LoginDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

    @Resource
    private LoginDao loginDao;

    @Override
    public Boolean checkLogin(LoginDto dto) {

        System.out.println(loginDao.findUserCountByIdAndPwd(dto));
        return loginDao.findUserCountByIdAndPwd(dto) == 1;
    }


}
