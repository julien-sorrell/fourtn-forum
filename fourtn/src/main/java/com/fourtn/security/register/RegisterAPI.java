package com.fourtn.security.register;

import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;


@RestController
@RequestMapping("/security/register")
public class RegisterAPI {
    @Resource
    private RegisterService registerService;

    @PostMapping ("")
    public Result registeruser(@RequestBody RegisterDto dto){
        System.out.println(dto);
        registerService.registeruser(dto);
        return Result.success();

    }


}
