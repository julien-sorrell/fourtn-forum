package com.fourtn.security.register;

public class RegisterDto {

    private String userid;
    private String username;
    private String usersex;
    private String password;
    private String usermail;
    private String useravatar;
    private String usersignature;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsersex() {
        return usersex;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsermail() {
        return usermail;
    }

    public void setUsermail(String usermail) {
        this.usermail = usermail;
    }

    public String getUseravatar() {
        return useravatar;
    }

    public void setUseravatar(String useravatar) {
        this.useravatar = useravatar;
    }

    public String getUsersignature() {
        return usersignature;
    }

    public void setUsersignature(String usersignature) {
        this.usersignature = usersignature;
    }

    @Override
    public String toString() {
        return "RegisterDto{" +
                "userid='" + userid + '\'' +
                ", username='" + username + '\'' +
                ", usersex='" + usersex + '\'' +
                ", password='" + password + '\'' +
                ", usermail='" + usermail + '\'' +
                ", useravatar='" + useravatar + '\'' +
                ", usersignature='" + usersignature + '\'' +
                '}';
    }
}
