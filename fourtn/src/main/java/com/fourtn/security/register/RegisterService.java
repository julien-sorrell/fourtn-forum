package com.fourtn.security.register;

import com.fourtn.common.Result;

public interface RegisterService {

    Result registeruser(RegisterDto dto);

}
