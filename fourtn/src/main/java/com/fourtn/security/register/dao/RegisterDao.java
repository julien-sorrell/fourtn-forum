package com.fourtn.security.register.dao;

import com.fourtn.security.register.RegisterDto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;


public interface RegisterDao {

    @Insert("insert into user(userid,username,usersex,password,usermail,useravatar,usersignature) values(#{userid},#{username},#{usersex},#{password},#{usermail},#{useravatar},#{usersignature})")
    void insertuser(RegisterDto dto);


}
