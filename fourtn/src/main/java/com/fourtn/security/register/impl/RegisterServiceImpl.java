package com.fourtn.security.register.impl;

import com.fourtn.common.Result;
import com.fourtn.security.register.dao.RegisterDao;
import com.fourtn.security.register.RegisterDto;
import com.fourtn.security.register.RegisterService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
@Service
@Transactional
public class RegisterServiceImpl implements RegisterService {
    @Resource
    RegisterDao registerDao;

    @Override
    public Result registeruser(RegisterDto dto) {

        registerDao.insertuser(dto);
        return Result.success();

    }

}
