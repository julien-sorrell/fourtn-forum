package com.fourtn.superuser.manage;

import com.fourtn.common.PageParam;

public class Manage extends PageParam {
    private String roleid;
    private String userid;

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

}
