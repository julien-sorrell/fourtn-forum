package com.fourtn.superuser.manage;

import com.fourtn.common.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/superuser/manage")
public class ManageAPI {
    //引入依赖对象
    @Resource //此注解表示从容器中引入依赖对象
    private ManageService manageService;

    @GetMapping("")
    public Result manageList(Manage dto) {
        Map<String,Object> page = manageService.getManageList(dto);
        return Result.success(page);
    }

    @PostMapping("")
    public Result insertManage(@RequestBody Manage dto){
        if(dto.getRoleid().equals("1")) {
            manageService.insertManage(dto);
            return Result.success();
        }
        else {
            return Result.fail(500,"不可新增全域管理员之外的角色");
        }
    }

    @DeleteMapping("/{roleid}/{userid}")
    public Result deleteManage(@PathVariable String roleid,@PathVariable String userid){
        if(roleid.equals("1")) {
            manageService.deleteManage(roleid, userid);
            return Result.success();
        }
        else {
            return Result.fail(500,"不可删除全域管理员之外的角色");
        }
    }
}
