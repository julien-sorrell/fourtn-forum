package com.fourtn.superuser.manage;

import java.util.Map;

public interface ManageService {

    Map<String,Object> getManageList(Manage dto);

    void insertManage(Manage dto);

    void deleteManage(String roleid,String userid);
}
