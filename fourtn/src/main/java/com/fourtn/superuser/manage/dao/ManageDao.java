package com.fourtn.superuser.manage.dao;

import com.fourtn.superuser.manage.Manage;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ManageDao {

    @Select("select * from confirmrole where roleid = 1")
    List<Manage> getManageList(Manage dto);

    @Insert("insert into confirmrole(roleid,userid) values(#{roleid}, #{userid})")
    void insertManage(Manage dto);

    @Delete("delete from confirmrole where roleid = #{roleid} and userid = #{userid}")
    void deleteManage(String roleid,String userid);
}
