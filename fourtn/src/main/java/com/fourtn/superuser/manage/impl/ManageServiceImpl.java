package com.fourtn.superuser.manage.impl;

import com.fourtn.common.Utils;
import com.fourtn.superuser.manage.Manage;
import com.fourtn.superuser.manage.ManageService;
import com.fourtn.superuser.manage.dao.ManageDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class ManageServiceImpl implements ManageService {

    @Resource
    private ManageDao manageDao;


    @Override
    public Map<String, Object> getManageList(Manage dto) {
        return Utils.getPage(dto, ()-> manageDao.getManageList(dto));
    }

    @Override
    public void insertManage(Manage dto) {
        manageDao.insertManage(dto);
    }

    @Override
    public void deleteManage(String roleid, String userid) {
        manageDao.deleteManage(roleid,userid);
    }
}
