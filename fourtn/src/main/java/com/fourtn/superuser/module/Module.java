package com.fourtn.superuser.module;

import com.fourtn.common.PageParam;

public class Module extends PageParam {
    private String roleid;
    private String moduleid;

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public String getModuleid() {
        return moduleid;
    }

    public void setModuleid(String moduleid) {
        this.moduleid = moduleid;
    }
}
