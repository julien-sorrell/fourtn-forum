package com.fourtn.superuser.module;

import com.fourtn.common.Result;
import com.fourtn.superuser.manage.Manage;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/superuser/module")
public class ModuleAPI {

    @Resource
    private ModuleService moduleService;

    @GetMapping("")
    public Result moduleList(Module dto) {
        Map<String,Object> page = moduleService.getModuleList(dto);
        return Result.success(page);
    }

    @PostMapping("")
    public Result insertModule(@RequestBody Module dto){
        if(dto.getRoleid().equals("1")) {
            moduleService.insertModule(dto);
            return Result.success();
        }
        else {
            return Result.fail(500,"不可新增全域管理员之外的角色权限");
        }
    }

    @DeleteMapping("/{roleid}/{moduleid}")
    public Result deleteModule(@PathVariable String roleid,@PathVariable String moduleid){
        if(roleid.equals("1")) {
            moduleService.deleteModule(roleid, moduleid);
            return Result.success();
        }
        else {
            return Result.fail(500,"不可删除全域管理员之外的角色权限");
        }
    }
}
