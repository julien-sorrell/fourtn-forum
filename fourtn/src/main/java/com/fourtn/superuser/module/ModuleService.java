package com.fourtn.superuser.module;

import java.util.Map;

public interface ModuleService {
    Map<String,Object> getModuleList(Module dto);

    void insertModule(Module dto);

    void deleteModule(String roleid,String moduleid);
}
