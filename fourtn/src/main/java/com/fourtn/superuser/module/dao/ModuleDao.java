package com.fourtn.superuser.module.dao;

import com.fourtn.superuser.module.Module;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ModuleDao {

    @Select("select * from rolemodule where roleid = 1")
    List<Module> getModuleList(Module dto);

    @Insert("insert into rolemodule(roleid,moduleid) values(#{roleid}, #{moduleid})")
    void insertModule(Module dto);

    @Delete("delete from rolemodule where roleid = #{roleid} and moduleid = #{moduleid}")
    void deleteModule(String roleid,String moduleid);
}
