package com.fourtn.superuser.module.impl;

import com.fourtn.common.Utils;
import com.fourtn.superuser.module.Module;
import com.fourtn.superuser.module.ModuleService;
import com.fourtn.superuser.module.dao.ModuleDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class ModuleServiceImpl implements ModuleService {

    @Resource
    private ModuleDao moduleDao;


    @Override
    public Map<String, Object> getModuleList(Module dto) {
        return Utils.getPage(dto, ()-> moduleDao.getModuleList(dto));
    }

    @Override
    public void insertModule(Module dto) {
        moduleDao.insertModule(dto);
    }

    @Override
    public void deleteModule(String roleid, String moduleid) {
        moduleDao.deleteModule(roleid,moduleid);
    }
}
