package com.fourtn.userfunction.bar;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Bar {

    public static final String BARAVATAR_DIR = "C:/touxiang";



    private String barid;
    private String userid;
    private String barname;
    private String baravatar;
    private Integer barhits;
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:SS")
    private Date bartime;

    private String barmark;
    private String bardescription;
    private Integer follownum;

    public String getBarmark() {
        return barmark;
    }

    public void setBarmark(String barmark) {
        this.barmark = barmark;
    }

    public Integer getFollownum() {
        return follownum;
    }

    public void setFollownum(Integer follownum) {
        this.follownum = follownum;
    }




    public String getBardescription() {
        return bardescription;
    }

    public void setBardescription(String bardescription) {
        this.bardescription = bardescription;
    }

    public String getBarid() {
        return barid;
    }

    public void setBarid(String barid) {
        this.barid = barid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBarname() {
        return barname;
    }

    public void setBarname(String barname) {
        this.barname = barname;
    }

    public String getBaravatar() {
        return baravatar;
    }

    public void setBaravatar(String baravatar) {
        this.baravatar = baravatar;
    }

    public Integer getBarhits() {
        return barhits;
    }

    public void setBarhits(Integer barhits) {
        this.barhits = barhits;
    }

    public Date getBartime() {
        return bartime;
    }

    public void setBartime(Date bartime) {
        this.bartime = bartime;
    }

}
