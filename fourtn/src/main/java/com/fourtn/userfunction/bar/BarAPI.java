package com.fourtn.userfunction.bar;


import com.fourtn.common.Result;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.List;

@RestController
@RequestMapping("/userfunction/bar")
public class BarAPI {
    @Resource
    private BarService barService;
    
    @RequestMapping("/bar_list")
    public Result barList(){

        return Result.success(barService.getBarList());

    }

    @RequestMapping("/bar_query")
    public Result barList(BarDto dto){
        return Result.success(barService.getBarList(dto));
    }


    @RequestMapping("bar_create")
    public void addBar(BarDto dto) {
        barService.createBar(dto);
    }

    @RequestMapping("bar_addfollow")
    public void addBarfollow(BarDto dto){
        barService.createBarFollow(dto);
    }

    @RequestMapping("bar_delfollow")
    public void delBarfollow(BarDto dto) {
        barService.deleteBarFollow(dto);
    }

    @RequestMapping("bar_apply")
    public void applyBar(BarExaminedDto dto) {
        barService.applicationBar(dto);
    }


    @GetMapping("/avatar/{filename}")
    public void getAvator(@PathVariable String filename, HttpServletResponse response) throws IOException {

        //读取文件的输入流
        InputStream in = new FileInputStream(Bar.BARAVATAR_DIR+"/"+filename);
        //将文件信息输出到浏览器的输出流
        OutputStream out = response.getOutputStream();

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while((len=in.read(b))!=-1){
            out.write(b,0,len);
        }
        out.flush();
        out.close();
        in.close();
    }


}
