package com.fourtn.userfunction.bar;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class BarDto {
    private String barid;
    private String userid;
    private String barname;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date bartime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date bartime_start;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date bartime_end;
    private String baravatar;
    private String bardescription;

    public String getBardescription() {
        return bardescription;
    }

    public void setBardescription(String bardescription) {
        this.bardescription = bardescription;
    }

    public String getBaravatar() {
        return baravatar;
    }

    public void setBaravatar(String baravatar) {
        this.baravatar = baravatar;
    }

    public String getBarid() {
        return barid;
    }

    public void setBarid(String barid) {
        this.barid = barid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBarname() {
        return barname;
    }

    public void setBarname(String barname) {
        this.barname = barname;
    }

    public Date getBartime() {
        return bartime;
    }

    public void setBartime(Date bartime) {
        this.bartime = bartime;
    }

    public Date getBartime_start() {
        return bartime_start;
    }

    public void setBartime_start(Date bartime_start) {
        this.bartime_start = bartime_start;
    }

    public Date getBartime_end() {
        return bartime_end;
    }

    public void setBartime_end(Date bartime_end) {
        this.bartime_end = bartime_end;
    }
}
