package com.fourtn.userfunction.bar;

import java.util.List;

public interface BarService {
    List<Bar> getBarList();

    List<Bar> getBarList(BarDto dto);

    void createBar(BarDto dto);

    void createBarFollow(BarDto dto);

    void deleteBarFollow(BarDto dto);

    void applicationBar(BarExaminedDto dto);
}
