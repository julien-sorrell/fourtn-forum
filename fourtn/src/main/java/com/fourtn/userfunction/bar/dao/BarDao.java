package com.fourtn.userfunction.bar.dao;

import com.fourtn.userfunction.bar.Bar;
import com.fourtn.userfunction.bar.BarDto;
import com.fourtn.userfunction.bar.BarExaminedDto;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BarDao {

    @Select("select follow.barid,barname,bardescription,bar.userid,baravatar,barhits,bartime,count(*) follownum from bar,follow where bar.barid=follow.barid GROUP BY bar.barid order by barhits desc")
    List<Bar> findBarList();

    @Select("select * from bar")
    List<Bar> findBarListByCondition(BarDto dto);

    @Insert("insert into bar(barid,barname,userid,bardescription,baravatar) values(#{barid},#{barname},#{userid},#{bardescription},#{baravatar})")
    void addBarList(BarDto dto);

    @Insert("insert into follow values(#{barid},#{userid})")
    void addBarFollow(BarDto dto);

    @Delete("delete from follow where barid = #{barid} and userid = #{userid}")
    void delBarFollow(BarDto dto);

    @Insert("insert into barexamined(barname,bardescription,userid) values(#{barname},#{bardescription},#{userid})")
    void addBarexamined(BarExaminedDto dto);
}
