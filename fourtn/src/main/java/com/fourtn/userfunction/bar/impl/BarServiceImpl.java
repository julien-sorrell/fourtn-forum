package com.fourtn.userfunction.bar.impl;

import com.fourtn.userfunction.bar.Bar;
import com.fourtn.userfunction.bar.BarDto;
import com.fourtn.userfunction.bar.BarExaminedDto;
import com.fourtn.userfunction.bar.BarService;
import com.fourtn.userfunction.bar.dao.BarDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class BarServiceImpl implements BarService {

    @Resource
    private BarDao barDao;

    @Override
    public List<Bar> getBarList() {
        return barDao.findBarList();
    }

    @Override
    public List<Bar> getBarList(BarDto dto) {
        return barDao.findBarListByCondition(dto);
    }

    @Override
    public void createBar(BarDto dto) {
        barDao.addBarList(dto);
    }

    @Override
    public void createBarFollow(BarDto dto) {
        barDao.addBarFollow(dto);
    }

    @Override
    public void deleteBarFollow(BarDto dto) {
        barDao.delBarFollow(dto);
    }

    @Override
    public void applicationBar(BarExaminedDto dto) {
        barDao.addBarexamined(dto);
    }


}
