package com.fourtn.userfunction.bbs;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fourtn.userfunction.reply.Reply;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Setter
@Getter
@ToString
public class Bbs {
    private String bbsid;       //格式为年月日+barid后三位+当前吧内最大贴子id+1
    private String barid;
    private String userid;
    private String bbsmark;
    private String bbstitle;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date bbstime;
    private Integer bbslike;
    private Integer bbsunlike;
    private Boolean bbstop;
    private Boolean bbsgood;
    private Integer bbshits;
    private Map<String,Object> page;        //包含replyList,reply中包含部分用户信息



}