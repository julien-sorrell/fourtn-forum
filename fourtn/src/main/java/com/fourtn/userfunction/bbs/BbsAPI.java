package com.fourtn.userfunction.bbs;
import com.fourtn.common.CommonService;
import com.fourtn.common.CurrentUser;
import com.fourtn.common.Result;
import com.fourtn.common.TokenUtils;
import com.fourtn.info.user.user.User;
import com.fourtn.userfunction.bar.Bar;
import com.fourtn.userfunction.bbs.enterbbs.EnterBbs;
import com.fourtn.userfunction.bbs.insertintobbs.InsertIntoBbs;
import com.fourtn.userfunction.bbs.userright.UbData;
import com.fourtn.userfunction.bbs.userright.UserRight;
import com.fourtn.userfunction.reply.Reply;
import com.fourtn.userfunction.reply.ReplyService;
import com.fourtn.userfunction.reply.insertintoreply.InsertIntoReply;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/userfunction/bbs")
public class BbsAPI {

    @Resource   //此注解表示从容器引入依赖对象
    private BbsService bbsService;

    @Resource
    private ReplyService replyService;

    @Resource
    private CommonService commonService;

    @PostMapping("/{bbsid}")         //判断是否是正确bbsid
    public Result isBbsid(@PathVariable String bbsid){
        if (bbsid == null || bbsid =="" ) return Result.fail(Result.ERR_CODE_BUSINESS,"贴子不存在");
        boolean hasbbs = bbsService.isBbsid(bbsid);
        if(!hasbbs){
            return Result.fail(Result.ERR_CODE_BUSINESS,"贴子不存在");
        }
        bbsService.addBbsHits(bbsid);
        return Result.success();
    }

    @PostMapping("/enterBbs")     //加载贴子数据,回复排序默认为楼层数从小到大顺序，当reply order为 true 时，按楼层倒序排序,每次访问该地址增加一次该贴子点击量
    public Result enterBbs(@RequestBody EnterBbs enterBbs){
        if (enterBbs.getBbsid() == null || enterBbs.getBbsid() =="") return Result.fail(Result.ERR_CODE_BUSINESS,"无效贴子id");
        return Result.success(bbsService.enterBbs(enterBbs));
    }

    @GetMapping("/bbsgetCurtUser")   //获取当前用户
    public Result bbsGetCurtUser(@RequestHeader("Token") String token){
        if(token == null || token.equals("")){      //游客身份
            return Result.success(new CurrentUser());
        }
        CurrentUser currentUser = TokenUtils.getUserInfo(token,commonService);
        return Result.success(currentUser);
    }

    @DeleteMapping("/bbsExit")      //用户退出
    public Result bbsExit(@RequestHeader("Token") String token){
        TokenUtils.removeToken(token);
        return Result.success();
    }

    @PostMapping("/postbbs")            //发贴并返回bbsid
    public Result postBbs(MultipartFile replypicture,String barid,String userid,String bbstitle,String bbsmark,String content)throws IOException {
        if(!bbsService.isBarid(barid)){
            return Result.fail(500,"吧不存在");
        }
        String banendtime = replyService.userIsBanned(userid);
        if(banendtime != null){
            return Result.fail(500,"您已被禁言,结束时间:"+banendtime);
        }

        InsertIntoBbs insertIntoBbs = new InsertIntoBbs();
        InsertIntoReply insertIntoReply = new InsertIntoReply();
        insertIntoBbs.setInsertIntoReply(insertIntoReply);
        insertIntoBbs.setBarid(barid);
        insertIntoBbs.setUserid(userid);
        insertIntoBbs.setBbstitle(bbstitle);
        insertIntoBbs.setBbsmark(bbsmark);
        insertIntoBbs.getInsertIntoReply().setContent(content);

        if(bbstitle == null || bbstitle.equals("")){
            return Result.fail(Result.ERR_CODE_BUSINESS,"贴子标题不能为空");
        }
        if(bbstitle.length() > 40){
            return Result.fail(Result.ERR_CODE_BUSINESS,"贴子标题不能超过40字");
        }

        if(content == null || content.equals(""))
        {
            return Result.fail(Result.ERR_CODE_BUSINESS,"回复内容不能为空");
        }
        if(content.length() > 300){
            return Result.fail(Result.ERR_CODE_BUSINESS,"回复不能超过300字");
        }
        if(replypicture == null || replypicture.equals("")){
            String bbsid  = bbsService.insertIntoBbs(insertIntoBbs);
            return Result.success("发贴成功",bbsid);
        }


        InputStream in = replypicture.getInputStream();

        String name = UUID.randomUUID().toString();//获得通过uuid算法得到的不重复的字符串
        String submitedFilename = replypicture.getOriginalFilename();//原始文件名
        String suffix = submitedFilename.substring(submitedFilename.lastIndexOf("."));
        String filename = name+suffix;


        OutputStream out = new FileOutputStream(Reply.REPLYPICTURE_DIR +"/"+filename);
        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while((len=in.read(b))!=-1){
            out.write(b,0,len);
        }
        out.flush();
        out.close();
        in.close();

        insertIntoBbs.getInsertIntoReply().setReplypicture(filename);
        String bbsid = bbsService.insertIntoBbs(insertIntoBbs);

        return Result.success("发贴成功",bbsid);
    }

    @PostMapping("/getbbscollection")          //贴子收藏数
    public Result getBbscollection(@RequestBody Bbs bbs){

        return Result.success(bbsService.getBbscollection(bbs.getBbsid()));
    }

    @GetMapping("/getbbsmarklist")          //贴子标签列表
    public Result getBbsmarkList(){
        return Result.success(bbsService.getBbsmarkList());
    }

    @PostMapping("/setUserRight")       //用户权限设置
    public Result setUserRight(@RequestBody UbData ubData){
        UserRight userRight = bbsService.setUserRight(ubData);
        return Result.success(userRight);
    }

    @GetMapping("/getBar/{barid}")      //获取当前吧信息
    public Result getBarByBarid(@PathVariable String barid){
        Bar bar = bbsService.getBarByBarid(barid);
        if(bar == null){
            return Result.fail(500,"吧不存在");
        }
        return Result.success(bar);
    }

    @GetMapping("/getBarAvatar/{filename}")     //获取吧头像
    public void getBarAvatar(@PathVariable String filename, HttpServletResponse response) throws IOException{
        if(filename == null || filename.equals("")) return;
        //读取文件的输入流
        InputStream in = new FileInputStream(Bar.BARAVATAR_DIR+"/"+filename);
        //将文件信息输出到浏览器的输出流
        OutputStream out = response.getOutputStream();

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while((len=in.read(b))!=-1){
            out.write(b,0,len);
        }
        out.flush();
        out.close();
        in.close();
    }

    @PutMapping("/addviews/{bbsid}")        //点击量加一
    public Result addViews(@PathVariable String bbsid){
        bbsService.addBbsHits(bbsid);
        return Result.success();
    }


    @PostMapping("/insert")             //用postbbs代替
    public void insertIntoBbs(@RequestBody InsertIntoBbs insertIntoBbs){
        bbsService.insertIntoBbs(insertIntoBbs);
    }

    @PostMapping("/delbbsbybbsid")        //接受前端参数bbsid，实现级联删除贴内所有回复
    public void delBbsByBbsid(@RequestBody BbsDto bbsDto){
        bbsService.delBbsByBbsid(bbsDto);
    }

    @RequestMapping("/bbs-list")        //该方法不完善，使用前需要修改
    public List<Bbs> bbsList(){
        return bbsService.getBbsList();
    }

    @RequestMapping("/bbs-query")       //该方法不完善，使用前需要修改
    public List<Bbs> bbsList(BbsDto dto){
        return bbsService.getBbsList(dto);
    }





}