package com.fourtn.userfunction.bbs;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class BbsDto {
    private String bbsid;
    private String barid;
    private String userid;
    private String bbsmark;
    private String bbstitle;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date bbstime;
    private Integer bbslike;
    private Integer bbsunlike;
    private Boolean bbstop;
    private Boolean bbsgood;
    private Integer bbshits;

}
