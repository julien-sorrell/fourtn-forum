package com.fourtn.userfunction.bbs;
import com.fourtn.userfunction.bar.Bar;
import com.fourtn.userfunction.bbs.enterbbs.EnterBbs;
import com.fourtn.userfunction.bbs.getbbsmark.GetBbsmark;
import com.fourtn.userfunction.bbs.insertintobbs.InsertIntoBbs;
import com.fourtn.userfunction.bbs.userright.UbData;
import com.fourtn.userfunction.bbs.userright.UserRight;

import java.util.List;


public interface BbsService {
    List<Bbs> getBbsList();

    List<Bbs> getBbsList(BbsDto dto);

    String insertIntoBbs(InsertIntoBbs insertIntoBbs);

    void delBbsByBbsid(BbsDto bbsDto);

    Bbs enterBbs(EnterBbs enterBbs);

    Boolean isBbsid(String bbsid);

    List<GetBbsmark> getBbsmarkList();

    Integer getBbscollection(String bbsid);

    UserRight setUserRight(UbData ubData);

    void addBbsHits(String bbsid);

    Bar getBarByBarid(String barid);

    Boolean isBarid(String barid);
}
