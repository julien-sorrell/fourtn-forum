package com.fourtn.userfunction.bbs.dao;

import com.fourtn.userfunction.bar.Bar;
import com.fourtn.userfunction.bbs.Bbs;
import com.fourtn.userfunction.bbs.BbsDto;
import com.fourtn.userfunction.bbs.enterbbs.EnterBbs;
import com.fourtn.userfunction.bbs.getbbsmark.GetBbsmark;
import com.fourtn.userfunction.bbs.insertintobbs.InsertIntoBbs;
import com.fourtn.userfunction.bbs.userright.UbData;
import com.fourtn.userfunction.reply.Reply;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
@Mapper
//@Mapper注解的的作用
//1:为了把mapper这个DAO交給Spring管理
//2:为了不再写mapper映射文件
//3:为了给mapper接口 自动根据一个添加@Mapper注解的接口生成一个实现类
public interface BbsDao {

    @Select("select * from bbs")
    List<Bbs> findBbsList();


    List<Reply> getReplyListByBbsid(String bbsid);

    List<Reply> getReplyListByEnterBbs(EnterBbs enterBbs);

    void insertIntoBbs(InsertIntoBbs insertIntoBbs);

    @Select("select ifnull(substring(max(convert(bbsid,unsigned)),12),0)+1 from bbs where barid = #{barid}")
    Integer getMaxBbsid(String barid);

    @Delete("delete from bbs where bbsid = #{bbsid}")
    void delBbsByBbsid(String bbsid);

    @Select("select * from bbs where bbsid = #{bbsid}")
    Bbs getBbsByBbsid(String bbsid);

    @Select("select count(*) from bbs where bbsid = #{bbsid}")
    Boolean isBbsid(String bbsid);

    @Select("select * from bbsmark")
    List<GetBbsmark> getBbsmarkList();

    @Select("select count(*) from collectbbs where bbsid = #{bbsid}")
    Integer getBbscollection(String bbsid);

    @Select("select ifnull(count(*),0) from role r,confirmrole c where c.userid = #{userid} and c.roleid = r.roleid and rolename = '全域管理员'")
    Integer isAdmin(String userid);

    @Select("select ifnull(count(*),0) from role r,confirmrole c where c.userid = #{userid} and c.roleid = r.roleid and rolename = '论坛管理员' and barid = #{barid}")
    Integer isBbsAdmin(UbData ubData);

    @Update("update bbs set bbshits = bbshits+1 where bbsid = #{bbsid}")
    void addBbsHits(String bbsid);

    @Select("select * from bar where barid = #{barid}")
    Bar getBarByBarid(String barid);

    @Select("select count(*) from bar where barid = #{barid}")
    boolean isBarid(String barid);
}

