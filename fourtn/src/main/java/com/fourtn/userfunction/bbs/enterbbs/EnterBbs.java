package com.fourtn.userfunction.bbs.enterbbs;

import com.fourtn.common.PageParam;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EnterBbs  extends PageParam {
    private String bbsid;
    private Boolean replyorder;         //回复排列顺序，默认false,按时间顺序排列，true按时间倒序
}