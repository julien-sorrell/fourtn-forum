package com.fourtn.userfunction.bbs.getbbsmark;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetBbsmark {
    private String bbsmark;
}
