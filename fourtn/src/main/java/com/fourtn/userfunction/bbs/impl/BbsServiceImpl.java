package com.fourtn.userfunction.bbs.impl;

import com.fourtn.common.Utils;
import com.fourtn.userfunction.bar.Bar;
import com.fourtn.userfunction.bbs.Bbs;
import com.fourtn.userfunction.bbs.BbsDto;
import com.fourtn.userfunction.bbs.BbsService;
import com.fourtn.userfunction.bbs.dao.BbsDao;
import com.fourtn.userfunction.bbs.enterbbs.EnterBbs;
import com.fourtn.userfunction.bbs.getbbsmark.GetBbsmark;
import com.fourtn.userfunction.bbs.insertintobbs.InsertIntoBbs;
import com.fourtn.userfunction.bbs.userright.UbData;
import com.fourtn.userfunction.bbs.userright.UserRight;
import com.fourtn.userfunction.reply.Reply;
import com.fourtn.userfunction.reply.dao.ReplyDao;
import com.fourtn.userfunction.reply.insertintoreply.InsertIntoReply;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BbsServiceImpl implements BbsService {
    @Resource
    private BbsDao bbsDao;

    @Resource
    private ReplyDao replyDao;


    @Override
    public List<Bbs> getBbsList() {
        List<Bbs> bbsList = bbsDao.findBbsList();

        for (Bbs bbs : bbsList) {
            List<Reply> replyList = bbsDao.getReplyListByBbsid(bbs.getBbsid());
//            bbs.setReplyList(replyList);
        }


        return bbsList;
    }

    @Override
    public List<Bbs> getBbsList(BbsDto dto) {
        return bbsDao.findBbsList();
    }

    @Override
    public String insertIntoBbs(InsertIntoBbs insertIntoBbs) {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        insertIntoBbs.setBbstime(now);

        String barid = insertIntoBbs.getBarid();
        Integer totalbbs = bbsDao.getMaxBbsid(barid);
        String bbsid =sdf.format(now)+barid.substring(barid.length()-3,barid.length())+totalbbs;

        insertIntoBbs.setBbsid(bbsid);

        if(insertIntoBbs.getBbsmark().equals(""))insertIntoBbs.setBbsmark(null);

        insertIntoBbs.setBbslike(0);
        insertIntoBbs.setBbsunlike(0);
        insertIntoBbs.setBbstop(false);
        insertIntoBbs.setBbsgood(false);
        insertIntoBbs.setBbshits(0);

        //确保第一个回复中的bbsid和userid一致
        InsertIntoReply insertIntoReply = insertIntoBbs.getInsertIntoReply();
        insertIntoReply.setBbsid(bbsid);
        insertIntoReply.setUserid(insertIntoBbs.getUserid());
        insertIntoReply.setReplytime(now);

        Integer currentfloor = replyDao.getMaxReplyFloor(insertIntoReply);
        insertIntoReply.setReplyfloor(currentfloor);

        String replyid = sdf.format(now)+bbsid.substring(bbsid.length()-3,bbsid.length())+currentfloor;
        insertIntoReply.setReplyid(replyid);



        //先插入贴子后插入回复
        bbsDao.insertIntoBbs(insertIntoBbs);
        replyDao.insertIntoReply(insertIntoReply);

        return insertIntoBbs.getBbsid();
    }

    @Override
    public void delBbsByBbsid(BbsDto bbsDto) {
        replyDao.delReplyByBbsid(bbsDto.getBbsid());
        bbsDao.delBbsByBbsid(bbsDto.getBbsid());
    }

    @Override
    public Bbs enterBbs(EnterBbs enterBbs) {
        Bbs currentBbs = null;
        currentBbs = bbsDao.getBbsByBbsid(enterBbs.getBbsid());

        currentBbs.setPage(Utils.getPage(enterBbs,()->bbsDao.getReplyListByEnterBbs(enterBbs)));
        return currentBbs;
    }

    @Override
    public Boolean isBbsid(String bbsid) {
        return bbsDao.isBbsid(bbsid);
    }

    @Override
    public List<GetBbsmark> getBbsmarkList() {
        return bbsDao.getBbsmarkList();
    }

    @Override
    public Integer getBbscollection(String bbsid) {
        return bbsDao.getBbscollection(bbsid);
    }

    @Override
    public UserRight setUserRight(UbData ubData) {
        UserRight userRight = new UserRight();

        Integer admin  = bbsDao.isAdmin(ubData.getUserid());        //全域管理员
        if(admin.equals(1)){
            userRight.setIsAdmin(true);
        }
        else{
            userRight.setIsAdmin(false);
        }

        Integer bbsadmin = bbsDao.isBbsAdmin(ubData);           //论坛管理员
        if(bbsadmin.equals(1)){
            userRight.setIsBbsAdmin(true);
        }
        else {
            userRight.setIsBbsAdmin(false);
        }
        return userRight;
    }

    @Override
    public void addBbsHits(String bbsid) {
        bbsDao.addBbsHits(bbsid);
    }

    @Override
    public Bar getBarByBarid(String barid) {
        return bbsDao.getBarByBarid(barid);
    }

    @Override
    public Boolean isBarid(String barid) {
        return bbsDao.isBarid(barid);
    }


}