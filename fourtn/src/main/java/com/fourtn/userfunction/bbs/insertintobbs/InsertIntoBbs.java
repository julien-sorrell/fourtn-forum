package com.fourtn.userfunction.bbs.insertintobbs;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fourtn.userfunction.reply.insertintoreply.InsertIntoReply;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class InsertIntoBbs {
    private String bbsid;
    private String barid;       //
    private String userid;      //
    private String bbsmark;
    private String bbstitle;        //
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date bbstime;
    private Integer bbslike;
    private Integer bbsunlike;
    private Boolean bbstop;
    private Boolean bbsgood;
    private Integer bbshits;
    private InsertIntoReply insertIntoReply;    //发帖时绑定的第一条回复
}
