package com.fourtn.userfunction.bbs.userright;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UbData {
    private String userid;
    private String barid;
}
