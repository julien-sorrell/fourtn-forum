package com.fourtn.userfunction.bbs.userright;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRight {
    private Boolean isAdmin;        //全域管理员
    private Boolean isBbsAdmin;     //论坛管理员
}
