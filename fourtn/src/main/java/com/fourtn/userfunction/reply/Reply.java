package com.fourtn.userfunction.reply;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;


@Getter
@Setter
@ToString
public class Reply {
    public static final String REPLYPICTURE_DIR="E:/Git/ReplyPicture";  //回复图片存储地址

    private String replyid;     //格式为年月日+贴子id后三位+当前贴子最大楼层数+1
    private String userid;
    private String bbsid;
    private String content;
    private String replypicture;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp replytime;
    private Integer replyfloor;
    private String username;
    private String useravatar;
    private String usersignature;


}