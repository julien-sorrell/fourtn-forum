package com.fourtn.userfunction.reply;

import com.fourtn.common.Result;
import com.fourtn.info.user.user.User;
import com.fourtn.userfunction.bbs.Bbs;
import com.fourtn.userfunction.bbs.BbsService;
import com.fourtn.userfunction.reply.insertintoreply.InsertIntoReply;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.UUID;

@Slf4j
@RestController //说明这是一个受spring容器管理的控制器组件(Bean)
@RequestMapping("/userfunction/reply") //本控制器的路径前缀
public class ReplyAPI {

    @Resource
    private ReplyService replyService;

    @Resource
    private BbsService bbsService;


    @GetMapping("/replypicture/{replyid}")      //发送回复图片给前端
    public void getReplyPicture(@PathVariable String replyid, HttpServletResponse response) throws IOException{
        String filename = replyService.getReplyPictureByReplyid(replyid);
        if(filename ==null || filename.equals(""))  return;
        InputStream in = new FileInputStream(Reply.REPLYPICTURE_DIR+"/"+filename);

        OutputStream out = response.getOutputStream();

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while((len=in.read(b))!=-1){
            out.write(b,0,len);
        }
        out.flush();
        out.close();
        in.close();
    }

    @PostMapping("/replypicture")           //插入回复，回复图片可为空，有禁言检查
    public Result uploadReplyPicture(MultipartFile replypicture,String userid,String bbsid,String content) throws IOException {
        if(!bbsService.isBbsid(bbsid)){
            return Result.fail(500,"贴子不存在");
        }
        String banendtime = replyService.userIsBanned(userid);
        if(banendtime != null){
            return Result.fail(500,"您已被禁言,结束时间:"+banendtime);
        }
        InsertIntoReply insertIntoReply = new InsertIntoReply();
        insertIntoReply.setUserid(userid);
        insertIntoReply.setBbsid(bbsid);
        insertIntoReply.setContent(content);
        if(insertIntoReply.getContent() == null || insertIntoReply.getContent().equals(""))return  Result.fail(Result.ERR_CODE_BUSINESS,"回复内容不能为空");

        if(replypicture ==null || replypicture.equals(""))
        {
            replyService.insertIntoReply(insertIntoReply);
            return Result.success();
        }

        InputStream in = replypicture.getInputStream();

        String name = UUID.randomUUID().toString();//获得通过uuid算法得到的不重复的字符串
        String submitedFilename = replypicture.getOriginalFilename();//原始文件名
        String suffix = submitedFilename.substring(submitedFilename.lastIndexOf("."));
        String filename = name+suffix;

        OutputStream out = new FileOutputStream(Reply.REPLYPICTURE_DIR+"/"+filename);

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while((len=in.read(b))!=-1){
            out.write(b,0,len);
        }
        out.flush();
        out.close();
        in.close();

        insertIntoReply.setReplypicture(filename);
        replyService.insertIntoReply(insertIntoReply);

        return Result.success();
    }

    @DeleteMapping("/delreplybyreplyid/{replyid}")          //可实现级联删除
    public Result delReply(@PathVariable String replyid, @RequestBody Bbs bbs){
        if(!bbsService.isBbsid(bbs.getBbsid())){
            return Result.fail(500,"贴子不存在");
        }
        if(!replyService.isReplyid(replyid)){
            return Result.fail(500,"回复不存在");
        }
        ReplyDto replyDto = new ReplyDto();
        replyDto.setReplyid(replyid);
        return Result.success(replyService.delReplyByReplyid(replyDto));
    }

    @PostMapping("/delreplybyreplyid")           //replyDto包含replyid即可，实现级联删除reportreply表中数据,实现删除首楼时删除贴子   该方法用上面代替
    public void delReplyByReplyid(@RequestBody ReplyDto replyDto){
        replyService.delReplyByReplyid(replyDto);
    }


    @GetMapping("/getuseravatar/{userid}")
    public void getUserAvatar(@PathVariable String userid, HttpServletResponse response)throws IOException{

        String filename = replyService.getUserAvatar(userid);

        if(filename == null || filename.equals(""))return;

        //读取文件的输入流
        InputStream in = new FileInputStream(User.AVATAR_DIR +"/"+filename);
        //将文件信息输出到浏览器的输出流
        OutputStream out = response.getOutputStream();

        byte[] b = new byte[10240];//容量为10240字节的缓存区
        int len = -1;//从输入流读取并放入缓存区的数据量

        while((len=in.read(b))!=-1){
            out.write(b,0,len);
        }
        out.flush();
        out.close();
        in.close();
    }


    @PostMapping("/insert")     //该方法用/replypicture代替
    public Result insertIntoReply(@RequestBody InsertIntoReply insertIntoReply){
        if(insertIntoReply.getContent() == null || insertIntoReply.getContent().equals(""))return  Result.fail(Result.ERR_CODE_BUSINESS,"回复内容不能为空");
        replyService.insertIntoReply(insertIntoReply);
        return Result.success("回复成功");
    }



    @RequestMapping("/test2")               //需要修改后再使用
    public List<Reply> replyList(){

        return replyService.getReplyList();

    }

    @RequestMapping("/replylist")           //需要修改后再使用
    public List<Reply> replyList(ReplyDto replyDto){

//        log.info("访问接口/userfunction/reply/test3");
        return replyService.getReplyList(replyDto);
    }


    @GetMapping("/getUserSignature/{userid}")
    public Result getUserSignature(@PathVariable String userid){
        String userSignature  = replyService.getUserSignature(userid);
        if (userSignature == null || userSignature.equals("")){
            userSignature = "该用户没有个人签名";
        }
        return Result.success("获得签名成功",userSignature);
    }

}