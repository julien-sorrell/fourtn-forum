package com.fourtn.userfunction.reply;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReplyDto {

    private String replyid;
    private String userid;
    private String bbsid;

}