package com.fourtn.userfunction.reply;

import com.fourtn.userfunction.reply.insertintoreply.InsertIntoReply;

import java.util.List;

public interface ReplyService {
    List<Reply> getReplyList();

    List<Reply> getReplyList(ReplyDto replyDto);

    void insertIntoReply(InsertIntoReply insertIntoReply);



    Boolean delReplyByReplyid(ReplyDto replyDto);

    String getReplyPictureByReplyid(String replyid);

    String getUserAvatar(String userid);

    String getUserSignature(String userid);

    Boolean isReplyid(String replyid);

    String userIsBanned(String userid);
}
