package com.fourtn.userfunction.reply.dao;

import com.fourtn.userfunction.reply.Reply;
import com.fourtn.userfunction.reply.ReplyDto;
import com.fourtn.userfunction.reply.insertintoreply.InsertIntoReply;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
public interface ReplyDao {

    @Select("select * from reply")
    List<Reply> getReplyList();

    List<Reply> getReplyListByCondition(ReplyDto replyDto);

    void insertIntoReply(InsertIntoReply insertIntoReply);

    @Select("select ifnull(max(replyfloor),0)+1 from reply where bbsid = #{bbsid}")
    Integer getMaxReplyFloor(InsertIntoReply insertIntoReply);

    @Delete("delete from reply where replyid = #{replyid}")
    void delReplyByReplyid(String replyid);

    @Delete("delete from reply where bbsid = #{bbsid}")
    void delReplyByBbsid(String bbsid);

    @Select("select replyfloor from reply where replyid = #{replyid}")
    Integer getReplyFloorByReplyid(String replyid);

    @Select("select bbsid from reply where replyid = #{replyid}")
    String getBbsidByReplyid(String replyid);

    @Select("select replypicture from reply where replyid = #{replyid}")
    String getReplyPictureByReplyid(String replyid);

    @Select("select useravatar from user where userid = #{userid}")
    String getUserAvatar(String userid);

    @Select("select usersignature from user where userid = #{userid}")
    String getUserSignature(String userid);

    @Select("select count(*) from reply where replyid = #{replyid}")
    Boolean isReplyid(String replyid);

    @Select("select max(endtime) from ban where bannedid = #{userid} and endtime >= #{time} and isvalid = true")
    String getBanEndtime(@Param("userid") String userid, @Param("time") String time);
}

