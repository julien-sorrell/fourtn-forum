package com.fourtn.userfunction.reply.impl;

import com.fourtn.userfunction.bbs.dao.BbsDao;
import com.fourtn.userfunction.reply.ReplyDto;
import com.fourtn.userfunction.reply.dao.ReplyDao;
import com.fourtn.userfunction.reply.Reply;
import com.fourtn.userfunction.reply.ReplyService;
import com.fourtn.userfunction.reply.insertintoreply.InsertIntoReply;
import com.fourtn.userfunction.reportreply.dao.ReportReplyDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class ReplyServiceImpl implements ReplyService {

    @Resource
    private ReplyDao replyDao;

    @Resource
    private ReportReplyDao reportReplyDao;

    @Resource
    private BbsDao bbsDao;

    @Override
    public List<Reply> getReplyList() {

        return replyDao.getReplyList();

    }

    @Override
    public List<Reply> getReplyList(ReplyDto replyDto) {

        return replyDao.getReplyListByCondition(replyDto);
    }

    @Override
    public void insertIntoReply(InsertIntoReply insertIntoReply) {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        try {
            insertIntoReply.setReplytime(now);

            Integer currentfloor = replyDao.getMaxReplyFloor(insertIntoReply);
            insertIntoReply.setReplyfloor(currentfloor);

            String bbsid = insertIntoReply.getBbsid();
            String replyid = sdf.format(now)+bbsid.substring(bbsid.length()-3,bbsid.length())+currentfloor;
            insertIntoReply.setReplyid(replyid);
            replyDao.insertIntoReply(insertIntoReply);

        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean delReplyByReplyid(ReplyDto replyDto) {
//        子表删除
        reportReplyDao.delByReplyid(replyDto.getReplyid());

        Integer replyfloor = replyDao.getReplyFloorByReplyid(replyDto.getReplyid());
        if (replyfloor.equals(1)){
            String bbsid = replyDao.getBbsidByReplyid(replyDto.getReplyid());
            replyDao.delReplyByBbsid(bbsid);    //删除一楼
            bbsDao.delBbsByBbsid(bbsid);    //删帖
             return true;
         }
        replyDao.delReplyByReplyid(replyDto.getReplyid());
         return false;
    }

    @Override
    public String getReplyPictureByReplyid(String replyid) {
        return replyDao.getReplyPictureByReplyid(replyid);
    }

    @Override
    public String getUserAvatar(String userid) {
        return replyDao.getUserAvatar(userid);
    }

    @Override
    public String getUserSignature(String userid) {
        return replyDao.getUserSignature(userid);
    }

    @Override
    public Boolean isReplyid(String replyid) {
        return replyDao.isReplyid(replyid);
    }

    @Override
    public String userIsBanned(String userid) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        String endtime = replyDao.getBanEndtime(userid,time);
        return endtime;
    }


}
