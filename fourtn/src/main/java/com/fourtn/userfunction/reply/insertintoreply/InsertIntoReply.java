package com.fourtn.userfunction.reply.insertintoreply;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;


import java.util.Date;

@Getter
@Setter
@ToString
public class InsertIntoReply {

    private String replyid;
    private String userid;
    private String bbsid;
    private String content;
    private String replypicture;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date replytime;
    private Integer replyfloor;

}
