package com.fourtn.userfunction.reportreply;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class ReportReply {
    private String reportid;    //记录id
    private String replyreporterid;  //注意是举报者id
    private String replyid;  //回复id，根据此可查询到被举报者id
    private String reportmark;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date reporttime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date reporthandletime;
    private Integer reportisdeleted;
    private String reportresult;



}
