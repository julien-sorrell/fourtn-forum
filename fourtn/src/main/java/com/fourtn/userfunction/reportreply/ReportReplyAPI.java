package com.fourtn.userfunction.reportreply;

import com.fourtn.userfunction.reply.Reply;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController //说明这是一个受spring容器管理的控制器组件(Bean)
@RequestMapping("/userfunction/reportreply") //本控制器的路径前缀
public class ReportReplyAPI {

    @Resource
    private ReportReplyService reportReplyService;

    @PostMapping("/delreportreply")     //删除一条举报记录,根据reportid删除
    public void delReportReply(@RequestBody ReportReplyDto reportReplyDto){
        reportReplyService.delReportReply(reportReplyDto);
    }

    @PostMapping("/delbyreplyid")       //不一定删除一条记录，可能是多条记录，根据replyid进行删除
    public void delByReplyid(@RequestBody ReportReplyDto reportReplyDto){
        reportReplyService.delByReplyid(reportReplyDto);
    }



    @RequestMapping("/test2")
    public List<ReportReply> getReportReplyList(ReportReplyDto reportReplyDto){
        log.info("访问接口/userfunction/reportreply/test2");
        return reportReplyService.getReportReplyList(reportReplyDto);
    }
}
