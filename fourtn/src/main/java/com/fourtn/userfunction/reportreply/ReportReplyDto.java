package com.fourtn.userfunction.reportreply;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class ReportReplyDto {
    private String reportid;
    private String replyreporterid;  //注意是举报者id
    private String replyid;  //回复id，根据此可查询到被举报者id
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date reporttime_start;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date reporttime_end;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date reporthandletime_start;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date reporthandletime_end;
    private Integer reportisdeleted;



}
