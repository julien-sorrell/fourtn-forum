package com.fourtn.userfunction.reportreply;

import java.util.List;

public interface ReportReplyService {
     List<ReportReply> getReportReplyList(ReportReplyDto reportReplyDto);

    void delByReplyid(ReportReplyDto reportReplyDto);

    void delReportReply(ReportReplyDto reportReplyDto);
}
