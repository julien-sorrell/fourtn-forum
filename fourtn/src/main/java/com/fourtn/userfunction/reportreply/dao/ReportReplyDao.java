package com.fourtn.userfunction.reportreply.dao;

import com.fourtn.userfunction.reportreply.ReportReply;
import com.fourtn.userfunction.reportreply.ReportReplyDto;
import org.apache.ibatis.annotations.Delete;

import java.util.List;

public interface ReportReplyDao {
    List<ReportReply> getReportReplyListByCondition(ReportReplyDto reportReplyDto);

    @Delete("delete from reportreply where replyid = #{replyid}")
    void delByReplyid(String replyid);

    @Delete("delete from reportreply where reportid = #{reportid}")
    void delReportReply(String reportid);
}
