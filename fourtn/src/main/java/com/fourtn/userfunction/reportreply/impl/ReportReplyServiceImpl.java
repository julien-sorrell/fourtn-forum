package com.fourtn.userfunction.reportreply.impl;

import com.fourtn.userfunction.reportreply.ReportReply;
import com.fourtn.userfunction.reportreply.ReportReplyDto;
import com.fourtn.userfunction.reportreply.ReportReplyService;
import com.fourtn.userfunction.reportreply.dao.ReportReplyDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service //说明这是一个受spring容器管理的业务组件(Bean)
@Transactional //说明本类的所有方法都是事务性
public class ReportReplyServiceImpl implements ReportReplyService {

    @Resource
    private ReportReplyDao reportReplyDao;

    @Override
    public List<ReportReply> getReportReplyList(ReportReplyDto reportReplyDto) {
        return reportReplyDao.getReportReplyListByCondition(reportReplyDto);
    }

    @Override
    public void delByReplyid(ReportReplyDto reportReplyDto) {
        reportReplyDao.delByReplyid(reportReplyDto.getReplyid());
    }

    @Override
    public void delReportReply(ReportReplyDto reportReplyDto) {
        reportReplyDao.delReportReply(reportReplyDto.getReportid());
    }
}
